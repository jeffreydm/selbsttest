/*
* Result Array structure:
* Base (array)         : [ Result, Result... ]
* -> Result (map)         : [ "ResultId" => # , "CategoryInfo" => CategoryInfo ]
* CategoryInfo (array) : [ Category, Category, Category ]
* Category (map)       : [ "CategoryId" => #, "Minimum" => #, "Maximum" => # ]
*/

xdescribe("testing check result", function() { //src: https://www.mkyong.com/javascript/how-to-access-json-object-in-javascript/
  var validTest=  {   //has to  be excluded because Jasmine doesn't work with AJAX
      "ResultId":0,
      "CategoryInfo":
      [{"CategoryId" : "1","Minimum" : "6", "Maximum" : "8"},
      {"CategoryId" : "2","Minimum" : "0", "Maximum" : "2"},
      {"CategoryId" : "3","Minimum" : "0", "Maximum" : "2"}]
    };

  var invalidTestMinimum= {
    "ResultId":1,
    "CategoryInfo":
    [{"CategoryId" : "1","Minimum" : "6", "Maximum" : "8"},
    {"CategoryId" : "2","Minimum" : "7", "Maximum" : "7"},
    {"CategoryId" : "3","Minimum" : "2", "Maximum" : "3"}]
  };

  var invalidTestMaximum= {
    "ResultId":2,
    "CategoryInfo":
    [{"CategoryId" : "1","Minimum" : "6", "Maximum" : "6"},
    {"CategoryId" : "2","Minimum" : "0", "Maximum" : "1"},
    {"CategoryId" : "3","Minimum" : "0", "Maximum" : "0"}]
  };

  var a=5;

  it("valid test", function() {
    expect(checkResult(validTest)).toEqual("Result is viable");
  });
  it("invalid test with a too high minimum", function() {
    expect(checkResult(invalidTestMinimum)).toEqual("The total of MINIMUM is more than 100%; invalid");
  });
  it("invalid test with a too high maximum", function() {
    expect(checkResult(invalidTestMaximum)).toEqual("The total of MAXIMUM is less than 100%; invalid");
  });
  it("invalid test with wrong datatype", function() {
    expect(checkResult(a)).toEqual("result to check contains incorrect Datatype");
  });


});
