xdescribe("testing checkExistingResult", function() { //has to  be excluded because Jasmine doesn't work with AJAX
var validTest=1;
var invalidTest="t";
var notExistingId=9999999;
var negativeId=-5;

  it("valid test", function() {
    expect(checkExistingResult(validTest)).not.toBe("ID not found");
  });
  it("valid test; exact result; CAN be wrong!", function() {
    expect(checkExistingResult(validTest)).toBe("Result is viable");
  });
  it("invalid test with a wrong variable datatype", function() {
    expect(checkExistingResult(invalidTest)).toEqual("invalid Datatype");
  });
  it("invalid test with a not existing ID", function() {
    expect(checkExistingResult(notExistingId)).toEqual("ID not found");
  });
  it("invalid test with a negative ID number", function() {
    expect(checkExistingResult(negativeId)).toEqual("invalid ID");
  });
});
