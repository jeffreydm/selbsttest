// VARIABLE DEFINITIONS

const getterPath = "/get_db_entries.php"

var quizDisabled = false;
var questionNumber;
var buttonWrapper;
var questionElement;
var nextQuestionElement;
var modal;
var btn;
var span;
var resultToCategoryMap;
var resultText;
var quizObject = new Array();
var allowedToClick = false;
quizObject.Category = new Array();




// START

start();

//start the quiz
function start() {
  //try to get the DB entries we need
  retrieveQuiz().then(function(resolved) {
    //if we got what we needed, set up the quiz
    setUp(resolved);
  }).catch(function(rejected) {
    //if some error occured, let the user know and keep them from clicking anything else
    disableQuiz(rejected);
  });
}



// AJAX METHODS

/* retrieve and parse the questions/answers
 * if successful, it sets up the quiz
 */
function retrieveQuiz() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'Question'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output));
        } catch (error) {
          console.log(output);
          reject("retrieveQuiz: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveQuiz: Connection Problem");
      }
    });

  });
}

/* retrieve and parse the description
 */
function retrieveDescription() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'Description'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output).Description);
        } catch (error) {
          console.log(output);
          reject("retrieveDescription: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveDescription: Connection Problem");
      }
    });

  });
}

/* retrieve and parse the description
 */
function retrieveButtonText() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'ButtonText'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output).ButtonText);
        } catch (error) {
          console.log(output);
          reject("retrieveButtonText: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveButtonText: Connection Problem");
      }
    });

  });
}

/* retrieve and parse the description
 */
function retrieveButtonLink() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'ButtonLink'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output).ButtonLink);
        } catch (error) {
          console.log(output);
          reject("retrieveButtonText: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveButtonText: Connection Problem");
      }
    });

  });
}

/* retrieve and parse all categories
 * fills a new hashmap with empty values to be used to count quizzer answers
 */
function retrieveCategories() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'Category'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output));
        } catch (error) {
          console.log(output);
          reject("retrieveCategories: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveCategories: Connection Problem");
      }
    });

  });
}

/* Get all the category ranges that match to a result
 */
function retrieveResults() {
  return new Promise(function(resolve, reject) {

    $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'Result_Category'
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output));
        } catch (error) {
          console.log(output);
          reject("retrieveResults: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveResults: Connection Problem");
      }
    });

  });
}

/* Get the result text for the given id
 */
function retrieveResultText(id) {
  return new Promise(function(resolve, reject) {

    return $.ajax({
      url: getterPath,
      type: 'GET',
      data: {
        'request': 'Result_Text',
        'id': id
      },
      timeout: 5000,
      success: function(output) {
        try {
          resolve(JSON.parse(output));
        } catch (error) {
          console.log(output);
          reject("retrieveResultText: Invalid JSON");
        }
      },
      fail: function() {
        reject("retrieveResultText: Connection Problem");
      }
    });

  });
}



// QUIZ MANIPULATON METHODS

/* sets up the quiz for the quizzer to take
 * takes the quiz questions as an object
 */
function setUp(quizQuestionsObject) {

  quizQuestionsObject.forEach(question => {
    if(question.Answers.length < 1) {
      disableQuiz("One of the Questions had no Answers");
    }
  });

  //set the Quiz Questions with a given Object
  quizObject.Question = quizQuestionsObject;


  //which question are we on?
  questionNumber = 0;

  //get Description on Startscreen from database
  retrieveDescription().then(function(resolved) {
    //add Description text to the right part in the html
    $('#startText p').text(resolved);

  }).catch(function(rejected) {
    disableQuiz(rejected);
  });

  //get Button Text on Endscreen
  retrieveButtonText().then(function(resolved) {
    //add Description text to the right part in the html
    $('#linkButton').text(resolved);

  }).catch(function(rejected) {
    disableQuiz(rejected);
  });

  //get Link of Button
  retrieveButtonLink().then(function(resolved) {
    //add link to Button
    $('#linkButtonWrapper').attr("href", resolved);
  }).catch(function(rejected) {
    disableQuiz(rejected);
  });

  //setup categories to accept quizzer answer selections (clicking an answer)
  retrieveCategories().then(function(resolved) {
    /* fill each category with an empty value
     * this is useful for evaluating the results
     * having exactly the set of categories as keys makes it easy to iterate
     */
    for (var category of resolved) {
      //to avoid overlap between get[reference] and get[id] we turn the ids into strings
      quizObject.Category["i" + category.Id] = 0;
    }
  }).catch(function(rejected) {
    disableQuiz(rejected);
  });

  //get HTML elements for setting content
  buttonWrapper = document.getElementById("buttons-wrapper");
  questionElement = document.getElementById("question");
  nextQuestionElement = document.getElementById("nextquestion");

  //The HELP (?) Button
  {
    // Get the modal
    modal = document.getElementById("modalHelp");
    // Get the button that opens the modal
    btn = document.getElementById("helpButton");
    // Get the <span> element that closes the modal
    span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
      modal.style.display = "block";
    }
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  //start the quiz
  triggerNextQuestion();

}

/* add an answer button
 * lets you set the id, text and value
 * this lets you determine which category each buttons increments
 */
function addAnswerButton(setId, setText, setValue) {
  var newButton = document.createElement('button');
  newButton.setAttribute('id', setId);
  newButton.setAttribute('value', "i" + setValue);
  newButton.innerHTML = setText;
  newButton.setAttribute('onClick', 'chooseAnswer(this.value)');
  buttonWrapper.appendChild(newButton);

  if ((questionNumber != 0)) {
    var questionBefore = quizObject.Question[questionNumber - 1];
    console.log("addAnswerButton: " + questionBefore.Answers.length);
  }else{
    var questionBefore = quizObject.Question[questionNumber ];
    console.log("addAnswerButton: " + questionBefore.Answers.length);
  }

  var antwortenAnzahl = questionBefore.Answers.length;

  //plays Animation to swipe in Answers
  swipeInAnswers(antwortenAnzahl);

  //wait 500 ms
  setTimeout(function() {
    /*then remove the animation class from the buttons, so that they wont move
    and are ready for the new animation class (swipe out)*/
    $("button#" + setId).removeClass("swipeInAnswers");
    allowedToClick = true;
  }, 500);

}

/* removes all answer buttons
 */
function removeAnswerButtons(anzahlAntworten) {
  for (i = 0; i < anzahlAntworten; i++) {
    //removes the actual html element
    document.getElementById(i).remove();
  }
}

/* selects an answer: when a quizzer clicks a button
 * increments the related category
 */
function chooseAnswer(selectedAnswer) {
  if (allowedToClick) {
    console.log("chosseAnswer: allowedToClick FALSE");
    allowedToClick = false;
    quizObject.Category[selectedAnswer] += 1;
    triggerNextQuestion();
  }
}




/*
 * do anything that is needed before we can load the next question buttons
 */
function triggerNextQuestion() {

  //if the quiz has not run into any problems
  if (!quizDisabled) {
    //if we have not answered all questions
    if (questionNumber < quizObject.Question.length) {
      console.log("triggerNextQuestion: fragenanzahl: " + quizObject.Question.length)
      //css cssanimation
      console.log("triggerNextQuestion: questionnumber: " + questionNumber);
      //löschen
      var question = quizObject.Question[questionNumber];
      var antwortenAnzahl = question.Answers.length;
      console.log("triggerNextQuestion: die frage" + questionNumber + "hat " + antwortenAnzahl +"Antworten");


      if ((questionNumber != 0)) {
        console.log("triggerNextQuestion: wird nur ausgeführt wenn nicht 0");
        /*start the animation for swiping in the new Questions and fading out
        the the old answers*/
        playAnimations();
      } else {
        /*wait unitl animations are done if therey any if there are none then
        load the next question*/
        RemoveAnswersAndWaitForAnimations();
      }

    } else { //if we _HAVE_ answered all questions
      //load the result
      retrieveResults().then(function(resolved) {
        resultToCategoryMap = resolved;
        loadResult();
      }).catch(function(rejected) {
        disableQuiz(rejected);
      });
    }
  }


}

/*Removes old Answers and waits until the Animations are then if there are any
 and then loads the next question*/
function RemoveAnswersAndWaitForAnimations() {
  if (questionNumber != 0) {
    var questionBefore = quizObject.Question[questionNumber -1];
  }else{
    var questionBefore = quizObject.Question[questionNumber];
  }
  var antwortenAnzahl = questionBefore.Answers.length;
  console.log("RemoveAnswersAndWaitForAnimations: anzahl an Antwoten die zu entfernen sind:" + antwortenAnzahl);

  //swipes out old Answers
  swipeOutAnswers(antwortenAnzahl);

  // if this is not th first question the wait until the animations are done
  if (questionNumber != 0) {
    //wait until the animations are done
    $('button#' + (antwortenAnzahl -1)).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
      function(e) {
        //actually load the next question
        loadNextQuestion();
      });
  } else {
    /*if this is the first question, dont wait for any animation then
     actually load the next question*/
    loadNextQuestion();
  }

}


/* displays the next question (NOTE:
 * if we have already answered all questions, this function loads the result)
 */
function loadNextQuestion() {

  //the current question (object) is
  var question = quizObject.Question[questionNumber];

  questionElement.textContent = question.Text;

  if (questionNumber != quizObject.Question.length - 1) {
    var questionNext = quizObject.Question[questionNumber + 1];
    nextQuestionElement.textContent = questionNext.Text;
  }
  var questionBefore;
  if ((questionNumber != 0)) {
    questionBefore = quizObject.Question[questionNumber - 1];
  }else{
    questionBefore = quizObject.Question[questionNumber];
  }
  var antwortenAnzahl = questionBefore.Answers.length;
  //calculation how long it takes to fully play the slide out animation
  var swipeOutAnimationTime;
    if(questionBefore.Answers.length == 1 ){
      swipeOutAnimationTime = 300;
    }else{
      swipeOutAnimationTime = ((300 + ((questionBefore.Answers.length -1) * 100)) - 500);
      if(swipeOutAnimationTime <= 400){
        swipeOutAnimationTime = 400;
      }
      console.log("animationtime: " + swipeOutAnimationTime + "Antwortenzahl" + antwortenAnzahl);
    }


  if ((questionNumber != 0)) {
    //TODO die Zeit berechen über animationsdauer und delay evtl dafür sorgen, dass question nicht zusammenklappt
  setTimeout(function() {
    for (var i = 0; i < question.Answers.length; i++) {
      addAnswerButton(i, question.Answers[i].Text, question.Answers[i].CategoryId);
    }
  }, swipeOutAnimationTime);
}else{
  for (var i = 0; i < question.Answers.length; i++) {
    addAnswerButton(i, question.Answers[i].Text, question.Answers[i].CategoryId);
  }
}



  //display our progress through the quiz
  refreshProgessBar(questionNumber);

  //increase question qounter by one
  questionNumber++;

  //do an invisible swap of the #oben div and the #unten div
  //also remove the animation class
  resetAnimations();

  //allow the user to click an answer
  console.log("jup");
  allowedToClick = true;
}


/* when the quiz is finished, calculate the resulting categories and connected
 * result.
 */
function loadResult() {
  document.getElementById('wrapperEnd').style.display = "block";
  document.getElementById('wrapper').style.display = "none";

  var logging = "";
  /* find the ID of the quizzers result
   * this method is a little complex, so here's an explanation:
   *   basically, for every result there is an entry per category,
   *   we iterate over every category and see if our answer percentages
   *   are in that range, and if it's correct for each category of
   *   a result, that's our result.
   */
  //have we found the correct result yet
  var stillSearching = true;
  //which result is it
  var theCorrectId;
  //for each result to category entry
  for (var resCat of resultToCategoryMap) {

    if (stillSearching) {
      //is this the correct result?
      var correctId = null;

      //get the all the categories
      var catInfoArr = resCat.CategoryInfo;
      //for each entry (so a set of category id/max/min)
      for (var j = 0; j < catInfoArr.length; j++) {
        var cat = catInfoArr[j];
        //check if the category is inside the parameters
        logging += "--" + resCat.ResultId + ":" + cat.CategoryId + " " +
          cat.Minimum + "<=" + quizObject.Category["i" + cat.CategoryId] + " AND " +
          cat.Maximum + ">=" + quizObject.Category["i" + cat.CategoryId] + "? ";
        if (cat.Minimum <= quizObject.Category["i" + cat.CategoryId] &&
          cat.Maximum >= quizObject.Category["i" + cat.CategoryId]) {
          //check if it has already been marked as incorrect
          if (correctId != false) {
            //otherwise, mark it as being right so far (if it is never marked false, it's our answer)
            logging += "Yes\n";
            correctId = true;
          } else {
            logging += "Irrelevant, already discounted\n";
          }
        } else {
          //if not, it can't be our answer
          logging += "No\n";
          correctId = false;
        }
      }

      //if every category matched
      if (correctId) {
        logging += "Our category is " + resCat.ResultId + "\n";
        theCorrectId = resCat.ResultId;
        stillSearching = false;
      }
    }
  }

  if (theCorrectId) {
    retrieveResultText(theCorrectId).then(function(resolved) {
      displayResult(resolved);
    }).catch(function(rejected) {
      disableQuiz(rejected);
    });;
  } else {
    var problemSet = "";
    for (var key in quizObject.Category) {
      problemSet + quizObject.Category[key];
    }
    console.log(logging);
    disableQuiz("Unable to find a matching Result");
  }
}

/* Display the final result
 * This is currently a very short method, but it is something that could be
 * subject to a lot of change, and it's nice to have a logical differentiation
 * between it and loading the result.
 */
function displayResult(resultText) {
  document.getElementById('resultText').innerHTML = resultText;
  confetti();
}



// SMALL HELPER METHODS

/* disable continuing with the quiz
 * called if there was a problem loading something
 */
function disableQuiz(reason) {
  quizDisabled = true;
  try {
    var button = document.getElementById("startButton");
    button.parentNode.removeChild(button);
  } catch (error) {
    //ignore
  }
  try {
    removeAnswerButtons();
    questionElement.textContent = "";
  } catch (error) {
    //ignore
  }
  alert("We're really sorry, unfortunately an error occurred:\n" + reason);
}




// FUNCTIONS RELATING TO THE VISUALS

//the function to disable the start screen and displaying the main screen
function showWrapper() {
  if(!quizDisabled) {
    $("html").css("background-color", "#65A3FF");
    document.getElementById('wrapper').style.display = "block";
    document.getElementById('wrapperStart').style.display = "none";
  }
}

/*
 * refreshes the value of the process bar for the current question and the
 * processbar for the next question
 */
function refreshProgessBar(currentQuestion) {
  //the calculated procentual value the process bar from the curent question should have
  var prozent = (((currentQuestion + 1) / quizObject.Question.length) * 100);
  //change the with of the processbar filling from the current question to the calculated value
  $('div#progressBarFill').width(prozent + '%');
  //the calculated procentual value the process bar from the next question should have
  var prozentNext = (((currentQuestion + 2) / quizObject.Question.length) * 100);
  //change the with of the processbar filling from the next question to the calculated value
  $('div#nextProgressBarFill').width(prozentNext + '%');
}

/*
 * animates the thumb of the endscreen
 */
$(function() {
  var scoreUploadSuccessAnimation = $('.score-upload-success');
  setInterval(animate, 500);

  setTimeout(function() {
    setInterval(stopAnimate, 2500);
  }, 500);

  function animate() {
    scoreUploadSuccessAnimation.attr('class', 'score-upload-success animate');
  }

  function stopAnimate() {
    scoreUploadSuccessAnimation.attr('class', 'score-upload-success');
  }
});

/*
 * if the start button has been clicked, position the answers
 * then allow the user to click on one of the answers
 */
$("#startButton").click(function() {
  repositionAnswers("start");
  allowedToClick = true;
});


/*
 * sets the answers buttons to the posstion the next question will have
 */
function repositionAnswers(AufgerufenVon) {
  //if this is called from the start button
  if (AufgerufenVon != "start") {
    //get the outerHeight of the next question
    var questionObenSize = $("#oben").outerHeight();
    // add 16 px to it for the spacing between the questions and the answers
    questionObenSize = questionObenSize + 16;
    //actually change the position of the answers in the css
    $("#buttons-wrapper").css("top", questionObenSize + "px");
    //repostion the Impressum
  //  repositionImpressum(AufgerufenVon);
  } else { //if this is not called from the start button
    //get the height of current question
    var questionUntenSize = $("#unten").outerHeight();
    // add 16 px to it for the spacing between the questions and the answers
    questionUntenSize = questionUntenSize + 16;
    //actually change the position of the answers in the css
    $("#buttons-wrapper").css("top", questionUntenSize + "px");
    //repostion the Impressum
  //  repositionImpressum(AufgerufenVon);
  }
}


/*
* resposition Datenschutz and Impressum repostion
*/
/*
function repositionImpressum(AufgerufenVon){
  if (AufgerufenVon != "start") {
  //get the outerHeight of the next question
  var questionObenSize = $("#oben").outerHeight();
  //get the outerHeight of the buttonWrapper
  var buttonWrapperSize = $("#buttons-wrapper").outerHeight();
  alert("buttonwrappersize " + buttonWrapperSize);
  // add 16 px to it for the spacing between the questions and the answers
  questionObenSize = questionObenSize + buttonWrapperSize + 46;
  //actually change the position of the answers in the css
  $("footer.mainfooter").css("top", questionObenSize + "px");
  } else {
    //if this is not called from the start button
     //get the height of current question
     var questionUntenSize = $("#unten").outerHeight();
     //get the outerHeight of the buttonWrapper
     var buttonWrapperSize = $("#buttons-wrapper").outerHeight();
     // add 16 px to it for the spacing between the questions and the answers
     questionUntenSize = questionUntenSize + buttonWrapperSize + 46;
     //actually change the position of the answers in the css
     $("footer.mainfooter").css("top", questionUntenSize + "px");


  }
}

*/

/*
* adds the animation class to swipe in the new question
*/
function swipeInNewQuestion() {
  //add the css animation class to the div with the id #oben
  $("#oben").addClass('SwipeInAnimation');
}

/*
* adds the animation class to shrink and fade out the old question
*/
function shrinkOldQuestion() {
  //add the css animation class to the div with the id #unten
  $("#unten").addClass('shrinkAnimation');
}

/*
* adds the animations class to swipe out the old answers
*/
function swipeOutAnswers() {

  if(questionNumber == 0){
    var question = quizObject.Question[questionNumber];
    var antwortenAnzahl = question.Answers.length;
    console.log("swpipeOutAnswers: Es wrden ausgeswipt und gelöscht :" + antwortenAnzahl);
  }
  else{
    var question = quizObject.Question[questionNumber -1];
    var antwortenAnzahl = question.Answers.length;
    console.log("swpipeOutAnswers: Es wrden ausgeswipt und gelöscht :" + antwortenAnzahl);
  }

  //loop throw the number of answer buttons that the question has
  for (i = 0; i < antwortenAnzahl; i++) {
    //add the css animation class to the div with the id #button + index of button
    $("button#" + i).addClass('swipeOutAnswers');
    /*
    * calculate a delay based on the index of the button so they will swipe out
    * one after another
    * Example:
    * button0 has index 0 : 0 * 100ms = 0ms delay
    * button1 has index 1 : 1 * 100ms = 100ms delay
    * button2 has index 2 : 2 * 200ms = 200ms delay
    */
    var delay = (i * 100);
    //acutally add the delay to the css
    $("button#" + i).css("animation-delay", delay + "ms");
  }
  //repostion the answerbuttons
  repositionAnswers();
  //wait until the animation of the last answerbutton is done
  $('button#' + (antwortenAnzahl - 1)).one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
    function(e) {
      console.log("!!!!swipe Animation vom letzten Button fertig?");
      //acutally remove the answerbuttons from the html
      removeAnswerButtons(antwortenAnzahl);
    });
}

/*
* adds the animations class to swipe In the new answers
*/
function swipeInAnswers(anzahlAntworten) {
  //disallow the user to click a button while the animations are playing
  console.log("SwipeInAnswers allowedToClick FALSE");
  allowedToClick = false;
  //loop throw the number of answer buttons that the question has
  for (i = 0; i < anzahlAntworten; i++) {
    $("button#" + i).addClass('swipeInAnswers');
    /*
    * calculate a delay based on the index of the button so they will swipe out
    * one after another
    * Example:
    * button0 has index 0 : 0 * 100ms = 0ms delay
    * button1 has index 1 : 1 * 100ms = 100ms delay
    * button2 has index 2 : 2 * 200ms = 200ms delay
    */
    var delay = (i * 100);
    //acutally add the delay to the css
    $("button#" + i).css("animation-delay", delay + "ms");
  }
}


/*
* play the animation for:
* -swiping in the new questions
* -skrinking and fading out the old question
* AND triggers the needed process to load the new answers
*/
function playAnimations() {
  //play the animation to swipe in the new question
  swipeInNewQuestion();
  //play the animation to swipe in the new question
  shrinkOldQuestion();
  //wait 300 ms so the animations have time to play
  setTimeout(function(e) {
    //triggers the needed process to load the new answers and remove the old answers
    RemoveAnswersAndWaitForAnimations();
  }, 300);
}

/*
* removes the animations classes of the div #unten and #oben
*/
function resetAnimations() {
  //remove the shrink animation clss from the div #unten
  $("#unten").removeClass("shrinkAnimation");
  //remoe the swipe in Animation from the div #oben
  $("#oben").removeClass("SwipeInAnimation");
}

/*
* reloads the page
*/

$('#restart').click(function() {
    location.reload();
});
