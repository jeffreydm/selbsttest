<?php

require_once '../php/classes/Membership.php';
$membership = New Membership();

$membership->confirm_Member();

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Selbst-Test Übersicht</title>

    <!--main css file-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!--css file for login-->
    <link href="style.css" rel="stylesheet" type="text/css">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1.0, user-scalable = no">
    <meta name="theme-color" content="#65A3FF" />
    <link rel="apple-touch-icon" sizes="57x57" href="../favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>
    <div id="modalWrapper">
      <div id="modalWrapper-content">
        <span class="close" onclick="closeModalHelp()">&times;</span>
      </div>
    </div>
    <nav class="horizontalNav">
      <div id="quizInfo">
        <div id="quizPicture"></div>
        <div id="quizName">SelbstTest</div>
      </div>

      <a href= "/login/index.php?status=loggedout">
        <button id="logout">Logout</button>
      </a>

    </nav>

    <nav class="vertikalNav">

      <ul class="navItems">
        <li><a href="/members/" class="current">Allgemein</a></li>
        <li><a href="/members/questions/">Fragen</a></li>
        <li><a href="/members/categories/">Kategorien</a></li>
        <li><a href="/members/results/">Ergebnisse</a></li>
      </ul>

    </nav>

    <div id="wrapper">
      <div class="innerWrapper">

        <h1>Allgemein</h1>

        <div class="rounded">
          <table class="overview_table">
                <tbody id='question_table_body'>
                </tbody>
              </table>
        </div>

      </div>
    </div>
  </body>

  <script src="/js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="helperJS.js"></script>
  <script type="text/javascript" src="general.js"></script>
</html>
