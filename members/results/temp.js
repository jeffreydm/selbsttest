//create an area for entering all Category min and max values
var categoriesMinMaxDiv = document.createElement("div");
categoriesMinMaxDiv.id = "categoriesMinMaxDiv";
formDiv.appendChild(categoriesMinMaxDiv);
//for every Category...
for(var i=0; i<dBCategories.length; i++) {
    var cat = dBCategories[i];

    //create a seperate div to make it easier to retrieve information later
    var categoryMinMaxDiv = document.createElement("div");
    categoryMinMaxDiv.class = "minMax";
    categoriesMinMaxDiv.appendChild(categoryMinMaxDiv);
    categoryMinMaxDiv.categoryId = cat.Id;

    //then add a text explanation of the field
    categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Minimum"));
    //and add an input for minimum
    var min = document.createElement("input");
    min.type = "number";
    min.class = "min";
    min.value = 0;
    min.min = 0;
    min.max = dBQuestionNumber;
    categoryMinMaxDiv.appendChild(min);

    //then add a text explanation of the field
    categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Maximum"));
    //and add an input for maximum
    var max = document.createElement("input");
    max.type = "number";
    max.class = "max";
    max.value = 0;
    max.min = 0;
    max.max = dBQuestionNumber;
    categoryMinMaxDiv.appendChild(max);
}

//define the behavior of clicking on submit (this gets weird)
submitButton = document.createElement("Button");
$(submitButton).addClass("fertigErgebnis");
submitButton.innerHTML = "Fertig";
submitButton.onclick = function() {
    wrapper.style.display = "none";
    //create a new FormData for easily building our request
    var formData = new FormData();

    //manually add our information to the form
    formData.append('request', 'Add_Result');
    formData.append('text', document.getElementById("resultText").value );
    var categoryMap = {};
    //also check if the entered information is valid
    var minTotal = 0;
    var maxTotal = 0;
    var minGreaterThanMax = false;
    var categoriesMinMaxDiv = document.getElementById("categoriesMinMaxDiv");
    for(var i=0; i<categoriesMinMaxDiv.childNodes.length; i++) {
        var categoryInfo = {};

        var tempMin = -1;
        var tempMax = -2;
        for(var j=0; j<categoriesMinMaxDiv.childNodes[i].childNodes.length; j++) {
            var node = categoriesMinMaxDiv.childNodes[i].childNodes[j];
            if(node.class == "min") {
                minTotal += parseInt(node.value);
                categoryInfo.min = node.value;
                tempMin = node.value;
            } else if (node.class == "max") {
                maxTotal += parseInt(node.value);
                categoryInfo.max = node.value;
                tempMax = node.value;
            } else {
                //do nothing
            }
        }
        if(tempMin > tempMax)
        minGreaterThanMax = true;

        categoryMap[categoriesMinMaxDiv.childNodes[i].categoryId] = categoryInfo;
    }
    formData.append('categoryMap', JSON.stringify(categoryMap));

    //validate if the information checks out
    if (minGreaterThanMax) {
        alert("Please check your entries, one of your minimum entries is greater than its maximum");
    } else if (minTotal>dBQuestionNumber) {
        alert("Please check your entries, the Category combination is not possible:\nThe minimum requirement exceeds the number of Questions");
    } else if (maxTotal<dBQuestionNumber) {
        alert("Please check your entries, the Category combination is not possible:\nThe maximum is less than the number of Questions");
    } else {
        //and if everything is ok, send our request
        editRequest(formData).then(function(resolved) {
            alert("Result was created");
        }).catch(function(rejected) {
            alert("The Result could not be created");
        });

        modal.parentNode.removeChild(modal);
    }
}
modal.appendChild(submitButton);

