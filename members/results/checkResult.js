function checkResult(resultToCheck){
  //"resultToCheck" expects following JSON Object with the following system:
  /*
  * Result Array structure:
  * Base (array)         : [ Result, Result... ]
  * -> Result (map)         : [ "ResultId" => # , "CategoryInfo" => CategoryInfo ]
  * CategoryInfo (array) : [ Category, Category, Category ]
  * Category (map)       : [ "CategoryId" => #, "Minimum" => #, "Maximum" => # ]
  */

  var minTotal=0;
  var maxTotal=0;
  var totalQuestions;
  //SELECT count( * ) as  total_record FROM student

  try{ //check if the datatype is correct
    var temp = resultToCheck.CategoryInfo[0].Minimum;
  }catch(err){                                      //source: https://www.mkyong.com/javascript/how-to-access-json-object-in-javascript/
    return "result to check contains incorrect Datatype";
  }
  $.ajax({ //query to get the total number of questions
    url: "/get_db_entries.php",
    type: "GET",
    data: {'request' : 'Question_Total'},

    success: function (output) {
      try{
        totalQuestions=JSON.parse(output);
      }
      catch (err) {
        console.log(err);
      }
      //calculation (in AJAX because this code is executed parallel to the code after the AJAX query)
      for(var i=0;i<Object.keys(resultToCheck).length;i++){ //https://stackoverflow.com/questions/5223/length-of-a-javascript-object
        minTotal+=Number(resultToCheck.CategoryInfo[i].Minimum);
        maxTotal+=Number(resultToCheck.CategoryInfo[i].Maximum);
      }
      if(minTotal>totalQuestions){ //the total of the minimum answerst must not be above 100%/the total number of questions
        return "The total of MINIMUM is more than 100%; invalid";
      }

      else if(maxTotal<totalQuestions){ //the total of the maximum must not be lower than 100%
        return "The total of MAXIMUM is less than 100%; invalid";
      }

      else{
        return "Result is viable";
      }
    }
  });

}

/*
* Result Array structure:
* -> Base (array)         : [ Result, Result... ]
* Result (map)         : [ "ResultId" => # , "CategoryInfo" => CategoryInfo ]
* CategoryInfo (array) : [ Category, Category, Category ]
* Category (map)       : [ "CategoryId" => #, "Minimum" => #, "Maximum" => # ]
*/

function checkExistingResult(id){ //this function checks an already existing result if it is (still) valid
  if(isNaN(id)){ //check if the entered parameter is viable
    return "invalid Datatype";
  }
  if(id<0){
    return "invalid ID";
  }

  var jsonArray;
  $.ajax({ //query to get all results
    url: "/get_db_entries.php",
    type: "GET",
    data: {'request' : 'Result_Category'},

    success: function (output) {
      try{
        jsonArray = JSON.parse(output);
      }
      catch (err) {
        console.log(err);
      } //filter for the one selected as parameter
      for(var i=0;i<Object.keys(jsonArray).length;i++){
        if(Number(jsonArray[i].ResultId)==id){
          //alert(checkResult(jsonArray[i]));
          return checkResult(jsonArray[i]);
        }
      }
      return "ID not found";
    }
  });
}
