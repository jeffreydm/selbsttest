var tableBody = document.getElementById('results_table_body');
var dBCategories = null;
var dBQuestionNumber = null;
var resultCategoryMapDefaults = [];

start();

//main function
function start() {
  //fill the table with all results
  dataMap = {'request':'Result'};
  getRequest(dataMap).then(function(resolved) {
    var results = resolved;

    //fill the result with all category information
    dataMap = {'request':'Result_Category'};
    getRequest(dataMap).then(function(resolved) {
      var categories = resolved;

      for(var i=0; i<results.length; i++) {
        var result = results[i];
        result.categories = [];

        //...find the corresponding Category information set and...
        var stillSearching = true;
        for(var j=0; j<categories.length; j++) {
          if(stillSearching && categories[j].ResultId == result.Id) {
            //... fill in our Minimum and Maximum values

            var subMap = [];
            for(var k=0; k<categories[j].CategoryInfo.length; k++) {
              var category = categories[j].CategoryInfo[k];

              result.categories[k] = category;
              subMap[category.CategoryId] = [category.Minimum, category.Maximum];
            }
            resultCategoryMapDefaults[result.Id] = subMap;
            stillSearching = false;
          }
        }
        //if we couldn't find a matching set of information, display an error
        if(stillSearching) {
          alert("Für das Ergebnis "+result.Id+" wurden keine Kategorie Informationen gefunden\n" + 
                "Die Anzeige wird nicht richtig funktionieren für diese Kategorie");

          //currently our solution is to simply display nothing
        }
      }

      setUp(results);
    }).catch(function(rejected) {
      console.log(rejected);
      alert("Die Datenbank konnte nicht erreicht werden");
      location.reload();
    });

  }).catch(function(rejected) {
    console.log(rejected);
    alert("Die Datenbank konnte nicht erreicht werden");
    location.reload();
  });

  //prepare dBCategories / dBQuestionNumber for adding new Results
  dataMap = {'request':'Category'};
  getRequest(dataMap).then(function(resolved) {

    dBCategories = resolved;
    dataMap = {'request':'Question_Total'};
    getRequest(dataMap).then(function(resolved) {
      dBQuestionNumber = resolved;

      //set up new Result button
      var button = document.getElementById("newResultButton");
      button.onclick = function() {
        newResultButton();
      }

    }).catch(function(rejected) {
      console.log(rejected);
    });
  }).catch(function(rejected) {
    alert("Could not retrieve Categories. You will not be able to create new Results.");
    console.log(rejected);
  });

  //set up the test coverage button
  testCoverageButton();
}

//general setup
function setUp(results) {
  //create all result rows
  for(var i=0; i<results.length; i++) {
    appendResultRow(results[i]);
  }
}

function appendResultRow(result) {
  var newRow = tableBody.insertRow();

  var id = newRow.insertCell(0);
  $(id).addClass("id");
  id.setAttribute("data-val", result.Id);
  var idVal = document.createTextNode(result.Id);
  id.appendChild(idVal);
  var text = newRow.insertCell(1);
  $(text).addClass("text");
  var textVal = document.createTextNode(result.Text);
  text.appendChild(textVal);

  var resultNumber = newRow.insertCell(2);
  resultNumber.innerHTML = "Ergebnis " + result.Id;
  $(resultNumber).addClass("resultNumber");
  $(resultNumber).addClass("resultNumber");
  var titleWidth = $(resultNumber).width();

  var buttonCell = newRow.insertCell(3);
  $(buttonCell).addClass("editButtonCellResult");
  $(buttonCell).css("left", 15 + titleWidth + 20);
  var button = document.createElement("button");
  $(button).addClass("editButton");
  button.onclick = function() {
    editResult( this.parentNode.parentNode.cells[0].innerHTML,
                      this.parentNode.parentNode.cells[1].innerHTML,
                      this );
  }
  buttonCell.appendChild(button);
  var removeResultButtonCell = newRow.insertCell(4);
  $(removeResultButtonCell).addClass("removeButtonCellResult");
  $(removeResultButtonCell).css("left", 15 + titleWidth + 40);
  var removeResultButton = document.createElement("button");
  $(removeResultButton).addClass("removeButton");
  removeResultButton.onclick = function() {
    deleteResult(this.parentNode.parentNode.cells[0].innerHTML, this.parentNode.parentNode);
  }
  removeResultButtonCell.appendChild(removeResultButton);


  var resultCategories = newRow.insertCell(5);
  for(var i=0; i<result.categories.length; i++) {
    var category = result.categories[i];
    var myString = category.CategoryId + ", Minimum:" + category.Minimum + ", Maximum:" + category.Maximum;
    var resultCategoryDiv = document.createElement("div");
    resultCategoryDiv.innerHTML = myString;
    $(resultCategoryDiv).addClass("resultMinMax");
    resultCategories.appendChild(resultCategoryDiv);
  }
}

/**
 * Button for creating new Results.
 * Opens a Modal which allows entering a Text, as well as minimum and maximum values for every Category.
 * Checks if the entered min and max are valid for a Result.
 * WARNING: DOES NOT CHECK FOR CONFLICTS WITH OTHER RESULTS
 */
function newResultButton() {
  //if the DB could not be loaded, die
  if(dBCategories === null || dBQuestionNumber === null) {
    alert("Die Datenbank konnte nicht erreicht werden");
    location.reload();
  } else {
    var wrapper = document.getElementById("modalWrapper");
    wrapper.style.display = "block";
    var modalWrapper = document.getElementById("modalWrapper-content");
    clearElement(modalWrapper);

    var modal = document.createElement("modal");
    modal.id = "addResultModal";

    var modalTitle = document.createElement("h2");
    $(modalTitle).addClass("modalTitle");
    modalTitle.innerHTML = "Neues Ergebnis";
    modal.appendChild(modalTitle);

    //create a real form, to get some formatting
    var form = document.createElement("form");
    form.id = "Form";
    modal.appendChild(form);

    //create text input
    var input = document.createElement("textarea");
    input.name = "text";
    input.type = "text";
    input.placeholder = "Neues Ergebnis eingeben";
    form.appendChild(input);

    //create a fake "form" extension (because we have to process the input manually later)
    var formDiv = document.createElement("div");
    formDiv.id = "addResultDiv";
    modal.appendChild(formDiv);

    //create an area for entering all Category min and max values
    var categoriesMinMaxDiv = document.createElement("div");
    categoriesMinMaxDiv.id = "categoriesMinMaxDiv";
    formDiv.appendChild(categoriesMinMaxDiv);
    //for every Category...
    for(var i=0; i<dBCategories.length; i++) {
      var cat = dBCategories[i];

      //create a seperate div to make it easier to retrieve information later
      var categoryMinMaxDiv = document.createElement("div");
      categoryMinMaxDiv.class = "minMax";
      categoriesMinMaxDiv.appendChild(categoryMinMaxDiv);
      categoryMinMaxDiv.categoryId = cat.Id;

      //then add a text explanation of the field
      categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Minimum"));
      //and add an input for minimum
      var min = document.createElement("input");
      min.type = "number";
      min.class = "min";
      min.value = 0;
      min.min = 0;
      min.max = dBQuestionNumber;
      categoryMinMaxDiv.appendChild(min);

      categoryMinMaxDiv.appendChild(document.createElement("br"));

      //then add a text explanation of the field
      categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Maximum"));
      //and add an input for maximum
      var max = document.createElement("input");
      max.type = "number";
      max.class = "max";
      max.value = 0;
      max.min = 0;
      max.max = dBQuestionNumber;
      categoryMinMaxDiv.appendChild(max);
    }

    //define the behavior of clicking on submit (this gets weird)
    submitButton = document.createElement("Button");
    $(submitButton).addClass("fertigErgebnis");
    submitButton.innerHTML = "Fertig";
    submitButton.onclick = function() {
      //create a new FormData for easily building our request
      var formData = new FormData( document.getElementById("Form") );

      //manually add our information to the form
      formData.append('request', 'Add_Result');
      var categoryMap = {};
      //also check if the entered information is valid
      var minTotal = 0;
      var maxTotal = 0;
      var minGreaterThanMax = false;
      var categoriesMinMaxDiv = document.getElementById("categoriesMinMaxDiv");
      for(var i=0; i<categoriesMinMaxDiv.childNodes.length; i++) {
        var categoryInfo = {};

        var tempMin = -1;
        var tempMax = -2;
        for(var j=0; j<categoriesMinMaxDiv.childNodes[i].childNodes.length; j++) {
          var node = categoriesMinMaxDiv.childNodes[i].childNodes[j];
          if(node.class == "min") {
            minTotal += parseInt(node.value);
            categoryInfo.min = node.value;
            tempMin = node.value;
          } else if (node.class == "max") {
            maxTotal += parseInt(node.value);
            categoryInfo.max = node.value;
            tempMax = node.value;
          } else {
            //do nothing
          }
        }
        if(tempMin > tempMax)
          minGreaterThanMax = true;

        categoryMap[categoriesMinMaxDiv.childNodes[i].categoryId] = categoryInfo;
      }
      formData.append('categoryMap', JSON.stringify(categoryMap));

      //validate if the information checks out
      if (minGreaterThanMax) {
        alert("Bitte überprüfen sie die Ergebnisse. Ein Minimum ist größer als ein Maximum.");
      } else if (minTotal>dBQuestionNumber) {
        alert("Bitte überprüfen sie die Ergebnisse:\nDas Minimum ist größer als die Menge an Fragen die es gibt.");
      } else if (maxTotal<dBQuestionNumber) {
        alert("Bitte überprüfen sie die Ergebnisse:\nDas Maximum ist kleiner als die Menge an Fragen die es gibt.");
      } else {
        //and if everything is ok, send our request
        wrapper.style.display = "none";
        editRequest(formData).then(function(resolved) {
          //get the Id, and then display the Result
          var map = {'request':'Last_Id', 'table':'Result'};
          getRequest(map).then(function(resolved) {
            var result = [];
            result.Id = resolved["MAX(Id)"];
            result.Text = formData.get("text");

            result.categories = []

            subMap = [];
            var k = 0;
            for(var id in categoryMap) {
              var category = [];
              category.CategoryId = id;
              category.Minimum = categoryMap[id].min;
              category.Maximum = categoryMap[id].max;

              result.categories[k] = category;
              subMap[category.CategoryId] = [category.Minimum, category.Maximum];
              k++;
            }
            resultCategoryMapDefaults[result.Id] = subMap;

            appendResultRow(result);
            alert("Das Ergebnis wurde erstellt");
          }).catch(function(rejected) {
            console.log(rejected);
            alert("Die Datenbank konnte nicht erreicht werden");
            location.reload();
          });
        }).catch(function(rejected) {
          console.log(rejected);
          alert("Das Ergebnis konnte nicht erstellt werden");
          location.reload();
        });

        modal.parentNode.removeChild(modal);
      }
    }
    modal.appendChild(submitButton);

    //simple cancel button
    cancelButton = document.createElement("Button");
    $(cancelButton).addClass("abbrechenErgebnis");
    cancelButton.innerHTML = "Abbrechen";
    cancelButton.onclick = function() {
      wrapper.style.display = "none";
      modal.parentNode.removeChild(modal);
    }
    modal.appendChild(cancelButton);

    modalWrapper.appendChild(modal);
  }
}
//Die Ersetzung für newResultButton()
//NOCH NICHT FERTIG
function betterNewResultButton() {
  //if the DB could not be loaded, die
  if(dBCategories === null || dBQuestionNumber === null) {
    alert("Die Datenbank konnte nicht erreicht werden");
    location.reload();
  } else {
    var requestList = [];

    var textRequest = {};
    textRequest.Type = "text";
    textRequest.TextRepresentation = "Neues Ergebnis eingeben";
    requestList.push(textRequest);

    for(var i=0; i<3; i++) {
      var numberRequest = {};
      numberRequest.Type = MODAL_TYPE_NUMB;

      numberRequest.Name = "Thing"+i;
      numberRequest.Min = 0;
      numberRequest.Max = dBQuestionNumber;
      numberRequest.Value = 0;
      numberRequest.ClassList = "min";

      requestList.push(numberRequest);

      var numberRequest = {};
      numberRequest.Type = MODAL_TYPE_NUMB;

      numberRequest.Name = "Thing"+i;
      numberRequest.Min = 0;
      numberRequest.Max = dBQuestionNumber;
      numberRequest.Value = 0;
      numberRequest.ClassList = "max";

      requestList.push(numberRequest);
    }

    buildAddModal( "Add_Result", "Neues Ergebnis",
      function(form) {
        //preVet
        console.log("preVet");
        return false;
      },
      function(form) {
        //success
        console.log("success");
      },
      function(form) {
        //failure
        console.log("failure");
      },
      requestList, []
    );
  }
}

function editResult(resultId, defaultText, buttonNode) {
  //if the DB could not be loaded, alert
  if(resultCategoryMapDefaults.length < 1 || dBQuestionNumber === null) {
    alert("Die Datenbank konnte nicht erreicht werden");
    location.reload();
  } else {
    var wrapper = document.getElementById("modalWrapper");
    wrapper.style.display = "block";
    var modalWrapper = document.getElementById("modalWrapper-content");
    clearElement(modalWrapper);

    var modal = document.createElement("modal");
    modal.id = "editResultModal";

    var modalTitle = document.createElement("h2");
    $(modalTitle).addClass("modalTitle");
    modalTitle.innerHTML = "Ergebnis Bearbeiten";
    modal.appendChild(modalTitle);

    //create a real form, to get some formatting
    var form = document.createElement("form");
    form.id = "Form";
    modal.appendChild(form);

    //create text input
    var input = document.createElement("textarea");
    input.name = "text";
    input.type = "text";
    input.placeholder = "Neues Ergebnis eingeben";
    input.value = defaultText;
    form.appendChild(input);

    //create a fake "form" extension (because we have to process the input manually later)
    var formDiv = document.createElement("div");
    formDiv.id = "editResultDiv";
    modal.appendChild(formDiv);

    //create an area for entering all Category min and max values
    var categoriesMinMaxDiv = document.createElement("div");
    categoriesMinMaxDiv.id = "categoriesMinMaxDiv";
    formDiv.appendChild(categoriesMinMaxDiv);
    //for every Category...
    for(var i=0; i<dBCategories.length; i++) {
      var cat = dBCategories[i];

      //create a seperate div to make it easier to retrieve information later
      var categoryMinMaxDiv = document.createElement("div");
      categoryMinMaxDiv.class = "minMax";
      categoriesMinMaxDiv.appendChild(categoryMinMaxDiv);
      categoryMinMaxDiv.categoryId = cat.Id;

      //then add a text explanation of the field
      categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Minimum"));
      //and add an input for minimum
      var min = document.createElement("input");
      min.type = "number";
      min.class = "min";
      min.value = resultCategoryMapDefaults[resultId][cat.Id][0];
      min.min = 0;
      min.max = dBQuestionNumber;
      categoryMinMaxDiv.appendChild(min);

      categoryMinMaxDiv.appendChild(document.createElement("br"));

      //then add a text explanation of the field
      categoryMinMaxDiv.appendChild(document.createTextNode("Kategorien "+cat.Id+" Maximum"));
      //and add an input for maximum
      var max = document.createElement("input");
      max.type = "number";
      max.class = "max";
      max.value = resultCategoryMapDefaults[resultId][cat.Id][1];
      max.min = 0;
      max.max = dBQuestionNumber;
      categoryMinMaxDiv.appendChild(max);
    }

    //define the behavior of clicking on submit (this gets weird)
    submitButton = document.createElement("Button");
    $(submitButton).addClass("fertigErgebnis");
    submitButton.innerHTML = "Fertig";
    submitButton.onclick = function() {
      //create a new FormData for easily building our request
      var formData = new FormData( document.getElementById("Form") );

      //manually add our information to the form
      formData.append('request', 'Edit_Result');
      formData.append('resultId', resultId);
      var categoryMap = {};
      //also check if the entered information is valid
      var minTotal = 0;
      var maxTotal = 0;
      var minGreaterThanMax = false;
      var categoriesMinMaxDiv = document.getElementById("categoriesMinMaxDiv");
      for(var i=0; i<categoriesMinMaxDiv.childNodes.length; i++) {
        var categoryInfo = {};

        var tempMin = -1;
        var tempMax = -2;
        for(var j=0; j<categoriesMinMaxDiv.childNodes[i].childNodes.length; j++) {
          var node = categoriesMinMaxDiv.childNodes[i].childNodes[j];
          if(node.class == "min") {
            minTotal += parseInt(node.value);
            categoryInfo.min = node.value;
            tempMin = node.value;
          } else if (node.class == "max") {
            maxTotal += parseInt(node.value);
            categoryInfo.max = node.value;
            tempMax = node.value;
          } else {
            //do nothing
          }
        }
        if(tempMin > tempMax)
          minGreaterThanMax = true;

        categoryMap[categoriesMinMaxDiv.childNodes[i].categoryId] = categoryInfo;
      }
      formData.append('categoryMap', JSON.stringify(categoryMap));

      //validate if the information checks out
      if (minGreaterThanMax) {
        alert("Bitte überprüfen sie die Ergebnisse. Ein Minimum ist größer als ein Maximum.");
      } else if (minTotal>dBQuestionNumber) {
        alert("Bitte überprüfen sie die Ergebnisse:\nDas Minimum ist größer als die Menge an Fragen die es gibt.");
      } else if (maxTotal<dBQuestionNumber) {
        alert("Bitte überprüfen sie die Ergebnisse:\nDas Maximum ist kleiner als die Menge an Fragen die es gibt.");
      } else {
        //and if everything is ok, send our request
      wrapper.style.display = "none";
        editRequest(formData).then(function(resolved) {
          tableElements = buttonNode.parentNode.parentNode.cells;
          //change the text
          tableElements[1].innerHTML = formData.get("text");
          //change the Min/Max display and cached values
          clearElement(tableElements[5]);
          subMap = [];
          var categories = [];
          var k = 0;
          for(var id in categoryMap) {
            var category = [];
            category.CategoryId = id;
            category.Minimum = categoryMap[id].min;
            category.Maximum = categoryMap[id].max;

            categories[k] = category;
            subMap[category.CategoryId] = [category.Minimum, category.Maximum];
            k++;
          }
          resultCategoryMapDefaults[resultId] = subMap;
          for(var i=0; i<categories.length; i++) {
            var myString = categories[i].CategoryId + ", Minimum:" + categories[i].Minimum + ", Maximum:" + categories[i].Maximum;
            var resultCategoryDiv = document.createElement("div");
            resultCategoryDiv.innerHTML = myString;
            $(resultCategoryDiv).addClass("resultMinMax");
            tableElements[5].appendChild(resultCategoryDiv);
          }

          alert("Das Ergebnis wurde bearbeitet");
        }).catch(function(rejected) {
          console.log(rejected);
          alert("Das Ergebnis konnte nicht bearbeitet werden");
          location.reload();
        });

        modal.parentNode.removeChild(modal);
      }
    }
    modal.appendChild(submitButton);

    //simple cancel button
    cancelButton = document.createElement("Button");
    $(cancelButton).addClass("abbrechenErgebnis");
    cancelButton.innerHTML = "Abbrechen";
    cancelButton.onclick = function() {
      wrapper.style.display = "none";
      modal.parentNode.removeChild(modal);
    }
    modal.appendChild(cancelButton);

    modalWrapper.appendChild(modal);
  }
}

function deleteResult(resultId, node) {
  if( confirm("Are you sure you want to delete Result "+resultId+"?") ) {

    var formData = new FormData();
    formData.append('request', 'Delete_Result');
    formData.append('resultId', resultId);
    editRequest(formData).then(function(resolved) {
      if(resolved) {
        deleteNode(node);
        alert("Ergebnis "+resultId+" wurde gelöscht");
      } else {
        console.log(resolved);
        alert("Das Ergebnis konnte nicht gelöscht werden");
        location.reload();
      }
    }).catch(function(rejected) {
      console.log(rejected);
      alert("Das Ergebnis konnte nicht gelöscht werden");
      location.reload();
    });

  } else {
    //do nothing
  }
}

function testCoverageButton() {
  var button = document.getElementById("resultCoverageButton");

  button.onclick = function() {
    $.ajax({
      url: "/php/classes/Validity.php",
      type: 'GET',
      data: {'request':'Validity'},
      timeout: 5000,
      success: function(output) {
        try {
          text = JSON.parse(output);
          alert(text);
        } catch (error) {
          console.log(output);
          alert("Die Datenbank konnte nicht erreicht werden");
          location.reload();
        }
      },
      fail: function() {
        alert("Die Datenbank konnte nicht erreicht werden");
      }
    });
  }
}
