const GETTER_PATH = "/get_db_entries.php";
const EDIT_PATH = "/members/editQuiz.php";

const MODAL_CONTENT_WRAPPER = "modalWrapper-content";
const MODAL_WRAPPER = "modalWrapper";

const MODAL_TYPE_TEXT = "text";
const MODAL_TYPE_SELE = "selector";
const MODAL_TYPE_NUMB = "category-range";


generalSetup();


//setup that needs to be done for all elements using helperJS.js
function generalSetup() {
      // Get the modal
    modal = document.getElementById("modalWrapper");
    // Get the <span> element that closes the modal
    span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

//clears a website element
function clearElement(element) {
    while(element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

/* gets information stored invisibly in a website element
 * WARNING: THIS WAS BUILT FOR A VERY SPECIFIC USE CASE
 */
function getRelativeInformation(element, request) {
    return element.parentNode.parentNode.cells[request].dataset.val;
}
/* sets information stored invisibly in a website element, and changes the visible part as well
 * WARNING: THIS WAS BUILT FOR A VERY SPECIFIC USE CASE
 */
function setRelativeInformation(element, request, value) {
    var cell = element.parentNode.parentNode.cells[request];
    cell.setAttribute("data-val", value);
    cell.innerHTML = value;
}
//a nicer way to delete a node than calling node.parentNode.removeChild(node) every time
function deleteNode(node) {
    node.parentNode.removeChild(node);
}

/**
 * generalized AJAX query as a function:
 * get_db_entries.php
 */
function getRequest(dataMap) {
    return new Promise(function(resolve, reject) {

        $.ajax({
            url: GETTER_PATH,
            method: 'GET',
            data: dataMap,
            timeout: 5000,
            success: function(output) {
                if(output==="") {
                    resolve([]);
                } else {
                    try {
                        resolve( JSON.parse(output) );
                    } catch (error) {
                        console.log(output);
                        reject("retrieveQuiz: Invalid JSON");
                    }
                }
            },
            fail: function() {
                reject("retrieveQuiz: Connection Problem");
            }
        });

    });
}

/**
 * generalized AJAX query as a function:
 * editQuiz.php
 */
/* STRUCTURE OF FORMDATA:
var formData = new FormData();
formData.append('request', 'REQUESTTYPE');
formData.append('REQUESTPARAM', PARAMVALUE);
editRequest(formData).then(function(resolved) {
  //DO STUFF
}).catch(function(rejected) {
  alert("ALERT ABOUT BAD THINGS");
});
 */
function editRequest(dataMap) {
    return new Promise(function(resolve, reject) {

        $.ajax({
                url: EDIT_PATH,
                method: 'POST',
                data: dataMap,
                contentType: false,
                processData: false,
                timeout: 5000,
                success: function(output) {
                    try {
                        resolve(output);
                    } catch (error) {
                        console.log(output);
                        reject("retrieveQuiz: Invalid JSON");
                    }
                },
                fail: function() {
                    reject("retrieveQuiz: Connection Problem");
                }
        });

    });
}

//generalized delete function to avoid redundancy
function deleteRequest(request, id, successText, failureText, node) {
    if( confirm("Sind sie sicher dass sie die ID "+id+" löschen wollen?") ) {
        var formData = new FormData();
        formData.append('request', request);
        formData.append('id', id);
        editRequest(formData).then(function(resolved) {
            if(resolved) {
                deleteNode(node);
                alert(successText);
            } else {
                console.log("inner failure");
                console.log(resolved);
                alert(failureText);
                location.reload();
            }
        }).catch(function(rejected) {
            console.log("outer failure");
            console.log(rejected);
            alert(failureText);
            location.reload();
        });
      } else {
            //do nothing
      }
}

/* generalized edit and add function to avoid redundancy
 * WARNING: THIS THING GOT A LITTLE COMPLICATED
 */
function buildAddModal(request, title, preVetFunction, successFunction, failureFunction, inputArray, additionalFormValues) {
    var modalWrapper = document.getElementById(MODAL_WRAPPER);
    modalWrapper.style.display = "block";

    var wrapper = document.getElementById(MODAL_CONTENT_WRAPPER);
    clearElement(wrapper);

    var modal = document.createElement("modal");
    modal.id = "addAnswerModal";

    //create Modal Title
    var modalTitle = document.createElement("h2");
    $(modalTitle).addClass("modalTitle");
    modalTitle.innerHTML = title;
    modal.appendChild(modalTitle);

    var form = document.createElement("form");
    form.id = "Form";
    modal.appendChild(form);

    inputArray.forEach(element => {
        switch(element.Type) {
            case MODAL_TYPE_TEXT:
                addTextInput(form, element);
                break;
            case MODAL_TYPE_SELE:
                addSelectorInput(form, element);
                break;
            case MODAL_TYPE_NUMB:
                addNumberRangeInput(form, element);
                break;
            default:
                alert("IMPROPER INPUT ARRAY ELEMENTS");
                break;
        }
    });

    submitButton = document.createElement("Button");
    $(submitButton).addClass("fertig");
    submitButton.innerHTML = "Fertig";
    submitButton.onclick = function() {
        var formData = new FormData( document.getElementById("Form") );

        //add any necessary elements to form data
        formData.append('request', request);
        additionalFormValues.forEach(element => {
            formData.append(element.Request, element.Value);
        });

        if(preVetFunction(formData)) {
            modalWrapper = document.getElementById("modalWrapper");
            modalWrapper.style.display = "none";
    
            editRequest(formData).then(function(resolved) {
                if(JSON.parse(resolved) === true) {
                    successFunction(formData);
                } else {
                    console.log("inner failure");
                    console.log(resolved);
                    failureFunction(formData);
                    location.reload();
                }
            }).catch(function(rejected) {
                console.log("outer failure");
                console.log(rejected);
                failureFunction(formData);
                location.reload();
            });
    
            deleteNode(modal);
        }
    }
    modal.appendChild(submitButton);

    cancelButton = document.createElement("Button");
    $(cancelButton).addClass("abbrechen");
    cancelButton.innerHTML = "Abbrechen";
    cancelButton.onclick = function() {
        modalWrapper = document.getElementById("modalWrapper");
        modalWrapper.style.display = "none";
        deleteNode(modal);
    }
    modal.appendChild(cancelButton);

    wrapper.appendChild(modal);
}
//helper function for buildAddModal, adds a text input to the modal
function addTextInput(form, element) {
    //create text input
    var input = document.createElement("textarea");
    input.name = "text";
    input.type = "text";
    input.placeholder = element.TextRepresentation;
    if (typeof element.DefaultText !== 'undefined') {
        input.value = element.DefaultText;
    }
    form.appendChild(input);
}
//helper function for buildAddModal, adds a selector input to the modal
function addSelectorInput(form, selectorInfo) {
    //create category selection
    var selector = document.createElement("select");
    //$(selector).addClass("categorySelector");
    selector.name = selectorInfo.Name;

    for(var i=0; i<selectorInfo.Options.length; i++) {
        var o = document.createElement("option");
        o.value = selectorInfo.Options[i].Value;
        o.text = selectorInfo.Options[i].Text;
        if (typeof selectorInfo.DefaultSelection !== 'undefined') {
            console.log(selectorInfo.DefaultSelection);
            if (o.value == selectorInfo.DefaultSelection) {
                o.selected = "selected";
            }
        }
        selector.appendChild(o);
    }
    form.appendChild(selector);
}
//helper function for buildAddModal, adds a number input to the modal
function addNumberRangeInput(form, numberInfo) {
    var input = document.createElement("input");
    input.type = "number";
    input.name = numberInfo.Name;
    input.min = numberInfo.Min;
    input.max = numberInfo.Max;
    input.value = numberInfo.Value;
    input.class = numberInfo.ClassList;
    form.appendChild(input);
}