const ID_CELL = 0;
const TITLE_CELL = 1;
const EDIT_BUTTON_CELL = 2;

const DELETE_CATEGORY_REQUEST = 'Delete_Category';

var tableBody = document.getElementById('categories_table_body');


start();


function start() {
  dataMap = {'request':'Category'};
  getRequest(dataMap).then(function(resolved) {
    setUp(resolved);
  }).catch(function(rejected) {
    console.log(rejected);
    alert("Die Datenbank konnte nicht erreicht werden");
  });

  newCategoryButton();
}

function setUp(rows) {
  for(var i=0; i<rows.length; i++) {
    appendCategoryRow(rows[i].Id, rows[i].Title);
  }
}

function appendCategoryRow(catId, catTitle) {
  var newRow = tableBody.insertRow();
  var id = newRow.insertCell(ID_CELL);
  $(id).addClass("id");
  id.setAttribute("data-val", catId);
  var idVal = document.createTextNode(catId);
  id.appendChild(idVal);
  var title = newRow.insertCell(TITLE_CELL);
  $(title).addClass("categoryName");
  title.setAttribute("data-val", catTitle);
  var titleVal = document.createTextNode(catTitle);
  title.appendChild(titleVal);
  var titleWidth = $(title).width();
  var editButtonCell = newRow.insertCell(EDIT_BUTTON_CELL);
  $(editButtonCell).addClass("editButtonCell");
  $(editButtonCell).css("left", 15 + titleWidth + 20);
  var editButton = document.createElement("Button");
  $(editButton).addClass("editButton");
  editButton.onclick = function() {
    editCategory( getRelativeInformation(this, ID_CELL),
                  getRelativeInformation(this, TITLE_CELL),
                  this );
  }
  editButtonCell.appendChild(editButton);
  var removeButtonCell = newRow.insertCell(3);
  $(removeButtonCell).addClass("removeButtonCell");
  $(removeButtonCell).css("left", 15 + titleWidth + 40);
  var removeButton = document.createElement("Button");
  $(removeButton).addClass("removeButton");
  removeButton.onclick = function() {
    var succ = "Die Kategorie wurde gelöscht. WARNUNG:\n" +
        "ERGEBNISSE KÖNNTEN JETZT INVALIDE SEIN, BITTE ÜBERPRÜFEN\n" +
        "ANTWORTEN KÖNNTEN JETZT INS LEERE FÜHREN, BITTE ÜBERPRÜFEN\n" +
        "Dies passiert wenn ein Ergebnis oder eine Antwort auf diese Kategorie hingewiesen war.";
    deleteRequest(  DELETE_CATEGORY_REQUEST, getRelativeInformation(this, ID_CELL),
                    succ, "Die Kategorie konnte nicht gelöscht werden", this.parentNode.parentNode );
  }
  removeButtonCell.appendChild(removeButton);
}

function newCategoryButton() {
  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Kategorie Name hier eingeben";
  var requestList = [request];

  var button = document.getElementById("newCategoryButton");
  button.onclick = function() {
    buildAddModal("Add_Category", "Neue Kategorie",
        function(form) {
          return true;
        },
        function(form) {
          //get the Id, and then display the Category
          var map = {'request':'Last_Id', 'table':'Category'};
          getRequest(map).then(function(resolved) {
            console.log(resolved);
            var theId = resolved["MAX(Id)"];
            appendCategoryRow(theId, form.get("text"));
            alert("Die Kategorie wurde erstellt");
          }).catch(function(rejected) {
            console.log(rejected);
            alert("Ein Fehler is aufgetreten");
          });
        },
        function(form) {
          console.log(form);
          alert("Die Kategorie konnte nicht erstellt werden");
        }, requestList, [] );
  }
}

function editCategory(id, defaultText, node) {
  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Kategorie Name hier eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  var additionRequest = {};
  additionRequest.Request = 'categoryId';
  additionRequest.Value = id;
  var additionRequestInfo = [additionRequest];

  buildAddModal("Edit_Category", "Kategorie Bearbeiten",
      function(form) {
        return true;
      },
      function(form) {
        setRelativeInformation(node, TITLE_CELL, form.get("text"));

        var title = $('td[data-val="'+ id +'"]').parent().find(".categoryName");
        titleWidth = $(title).width();
        console.log(id);
        var editButtonCell = $('td[data-val="'+ id +'"]').parent().find(".editButtonCell");
        $(editButtonCell).css("left", 15 + titleWidth + 20);
        var removeButtonCell = $('td[data-val="'+ id +'"]').parent().find(".removeButtonCell");
        $(removeButtonCell).css("left", 15 + titleWidth + 40);

        alert("Die Kategorie wurde bearbeitet");
      },
      function(form) {
        console.log(form);
        alert("Die Kategorie konnte nicht bearbeitet werden");
      }, requestList, additionRequestInfo);
}

function reload() {
  //TODO
}
