const QUESTION_NUMBER_CELL = 0;
const QUESTION_ID_CELL = 1;
const QUESTION_TEXT_CELL = 2;
const QUESTION_ANSWERS_TABLE_CELL = 3;
const QUESTION_REMOVE_BUTTON_CELL = 4;
const QUESTION_EDIT_BUTTON_CELL = 5;

const ANSWER_NUMBER_CELL = 0;
const ANSWER_ID_CELL = 1;
const ANSWER_TEXT_CELL = 2;
const ANSWER_CATEGORY_CELL = 3;
const ANSWER_EDIT_BUTTON_CELL = 4;
const ANSWER_REMOVE_BUTTON_CELL = 5;

const QUESTION_DELETE_REQUEST = 'Delete_Question';
const ANSWER_DELETE_REQUEST = 'Delete_Answer';

var tableBody = document.getElementById('question_table_body');

start();

function start() {
  dataMap = {'request':'Question'};
  getRequest(dataMap).then(function(resolved) {
    setUp(resolved);
  }).catch(function(rejected) {
    alert("There was a problem setting up");
    console.log(rejected);
  });

  //set up new Question button
  newQuestionButton();

  // Get the modal
  modal = document.getElementById("modalWrapper");
  // Get the <span> element that closes the modal
  span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}

//displays all questions in the HTML table
function setUp(questions) {
  for(var i=0; i<questions.length; i++) {
    appendQuestionRow(questions[i]);
  }
}

function appendQuestionRow(question) {
  var newRow = tableBody.insertRow();

  var questionNumber = newRow.insertCell(QUESTION_NUMBER_CELL);
  $(questionNumber).addClass("questionNumber");
  var questionNumberVal = document.createTextNode("Frage " + question.Id );
  questionNumber.appendChild(questionNumberVal);
  var titleWidth = $(questionNumber).width();

  var id = newRow.insertCell(QUESTION_ID_CELL);
  $(id).addClass("id");
  id.setAttribute("data-val", question.Id);
  var idVal = document.createTextNode("ID: "+question.Id);
  id.appendChild(idVal);
  var text = newRow.insertCell(QUESTION_TEXT_CELL);
  text.setAttribute("data-val", question.Text);
  $(text).addClass("text");
  var textVal = document.createTextNode(question.Text);
  text.appendChild(textVal);
  var answers = newRow.insertCell(QUESTION_ANSWERS_TABLE_CELL);
  $(answers).addClass("answers");
  //removeButton
  var removeButtonCell = newRow.insertCell(QUESTION_REMOVE_BUTTON_CELL);
  $(removeButtonCell).addClass("removeButtonCell");
  $(removeButtonCell).css("left", 15 + titleWidth + 40);
  var removeButton = document.createElement("Button");
  $(removeButton).addClass("removeButton");
  removeButton.onclick = function() {
    deleteRequest(  QUESTION_DELETE_REQUEST, getRelativeInformation(this, QUESTION_ID_CELL),
                    "Die Frage wurde gelöscht", "Die Frage konnte nicht gelöscht werden",
                    this.parentNode.parentNode );
  }
  removeButtonCell.appendChild(removeButton);

  var editQuestionButtonCell = newRow.insertCell(QUESTION_EDIT_BUTTON_CELL);
  $(editQuestionButtonCell).addClass("editButtonCell");
  $(editQuestionButtonCell).css("left", 15 + titleWidth + 20);
  var editQuestionButton = document.createElement("Button");
  $(editQuestionButton).addClass("editButton");
  editQuestionButton.onclick = function() {
    editQuestion( getRelativeInformation(this, QUESTION_ID_CELL),
                  getRelativeInformation(this, QUESTION_TEXT_CELL),
                  this );
  }
  editQuestionButtonCell.appendChild(editQuestionButton);

  //toggle Button
  var toggleButtonCell = newRow.insertCell(4);
  $(toggleButtonCell).addClass("toggleButtonCell");
  var toggleButton = document.createElement("Button");
  $(toggleButton).addClass("toggleButton");
  toggleButton.onclick = function() {
    toggleShowHide(this);
  }
  toggleButtonCell.appendChild(toggleButton);


  //create an HTML table for all answers to the current question
  var table = document.createElement('table');
  $(table).addClass("answer_table");
  {
    var addAnswerButton = document.createElement("Button");
    $(addAnswerButton).addClass("addAnswerButton");
    $(addAnswerButton).addClass("newButton");
    addAnswerButton.onclick = function() {
      addAnswerDialog( getRelativeInformation(this.parentNode, QUESTION_ID_CELL),
                       this.parentNode );
    }
    table.appendChild(addAnswerButton);
    $(addAnswerButton).text("+");
  }
  answers.appendChild(table);

  //fill table with answer information
  for(var j=0; j<question.Answers.length; j++) {
    var answer = question.Answers[j];
    appendAnswerRow(table, answer);
  }
}

function appendAnswerRow(appendNode, answer) {
  var newRow = appendNode.insertRow();

  //displays "Antwort" and the number of the Answer
  var answerNumber = newRow.insertCell(ANSWER_NUMBER_CELL);
  $(answerNumber).addClass("answerNumber");
  var answerNumberVal = document.createTextNode("Antwort " + answer.Id );
  answerNumber.appendChild(answerNumberVal);

  var id = newRow.insertCell(ANSWER_ID_CELL);
  id.setAttribute("data-val", answer.Id);
  $(id).addClass("id");
  var idVal = document.createTextNode("ID: "+answer.Id);
  id.appendChild(idVal);
  var text = newRow.insertCell(ANSWER_TEXT_CELL);
  text.setAttribute("data-val", answer.Text);
  $(text).addClass("answerText");
  var textVal = document.createTextNode(answer.Text);
  text.appendChild(textVal);
  var categoryId = newRow.insertCell(ANSWER_CATEGORY_CELL);
  categoryId.setAttribute("data-val", answer.CategoryId);
  $(categoryId).addClass("categoryId");
  var categoryIdVal = document.createTextNode("Kategorie: " + answer.CategoryId);
  categoryId.appendChild(categoryIdVal);

  var editAnswerCell = newRow.insertCell(ANSWER_EDIT_BUTTON_CELL);
  $(editAnswerCell).addClass("editButtonCell");
  $(editAnswerCell).addClass("editAnswerCell");
  var editAnswerButton = document.createElement("Button");
  $(editAnswerButton).addClass("editButton");
  $(editAnswerButton).addClass("editAnswerButton");
  editAnswerButton.onclick = function() {
    editAnswer( getRelativeInformation(this, ANSWER_ID_CELL),
                getRelativeInformation(this, ANSWER_TEXT_CELL),
                getRelativeInformation(this, ANSWER_CATEGORY_CELL),
                this );
  }
  editAnswerCell.appendChild(editAnswerButton);
  var removeAnswerCell = newRow.insertCell(ANSWER_REMOVE_BUTTON_CELL);

  $(removeAnswerCell).addClass("removeButtonCell");
  $(removeAnswerCell).addClass("removeButtonCellAnswers");

  var removeAnswerButton = document.createElement("Button");
  $(removeAnswerButton).addClass("removeButton");
  removeAnswerButton.onclick = function() {
    deleteRequest(  ANSWER_DELETE_REQUEST, getRelativeInformation(this, ANSWER_ID_CELL),
                "Answer was deleted", "The Answer could not be deleted",
                this.parentNode.parentNode );
  }
  removeAnswerCell.appendChild(removeAnswerButton);
}

function newQuestionButton() {
  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neue Frage eingeben";
  var requestList = [request];

  var button = document.getElementById("newQuestionButton");
  button.onclick = function() {
    buildAddModal( "Add_Question", "Neue Frage",
        function(form) {
          return true;
        },
        function(form) {
          //get the Id, and then display the Question
          var map = {'request':'Last_Id', 'table':'Question'};
          getRequest(map).then(function(resolved) {
            var question = [];
            question.Id = resolved["MAX(Id)"];
            question.Text = form.get("text");
            question.Answers = [];
            appendQuestionRow(question);
            alert("Die Frage wurde erstellt");
          }).catch(function(rejected) {
            console.log(rejected);
            alert("Die Datenbank konnte nicht erreicht werden");
            location.reload();
          });
        },
        function(form) {
          console.log(form);
          alert("Die Frage konnte nicht erstellt werden");
        }, requestList, [] );
  }
}

function editQuestion(questionId, defaultText, node) {
  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neuer Fragentext eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  var additionRequest = {};
  additionRequest.Request = 'questionId';
  additionRequest.Value = questionId;
  var additionRequestInfo = [additionRequest];

  buildAddModal("Edit_Question", "Neue Frage",
      function(form) {
        return true;
      },
      function(form) {
        setRelativeInformation(node, QUESTION_TEXT_CELL, form.get("text"));
        alert("Die Frage wurde bearbeitet");
      },
      function(form) {
        console.log(form);
        alert("Die Frage konnte nicht bearbeitet werden");
      }, requestList, additionRequestInfo);
}

function addAnswerDialog(questionId, appendNode) {
  var categoryRequest = {'request':'Category'};
  getRequest(categoryRequest).then(function(resolved) {
    var categoryList = [];

    for(var i=0; i<resolved.length; i++) {
      var option = {};
      option.Value = resolved[i].Id;
      option.Text = resolved[i].Title + " (" + resolved[i].Id + ")";
      categoryList[i] = option;
    }

    var textRequest = {};
    textRequest.Type = MODAL_TYPE_TEXT;
    textRequest.TextRepresentation = "Neue Antwort eingeben";

    var selectRequest = {};
    selectRequest.Type = MODAL_TYPE_SELE;
    selectRequest.Name = "categoryId";
    selectRequest.Options = categoryList;


    var requestList = [textRequest, selectRequest];

    var additionRequest = {};
    additionRequest.Request = 'questionId';
    additionRequest.Value = questionId;
    var additionRequestInfo = [additionRequest];

    buildAddModal("Add_Answer", "Neue Antwort",
        function(form) {
          return true;
        },
        function(form) {
          //get the Id, and then display the Answer
          var map = {'request':'Last_Id', 'table':'Answer'};
          getRequest(map).then(function(resolved) {
            var newAnswer = [];
            newAnswer.Id = resolved["MAX(Id)"];
            newAnswer.Text = form.get("text");
            newAnswer.CategoryId = form.get(selectRequest.Name);
            appendAnswerRow(appendNode, newAnswer);
            alert("Die Antwort wurde erstellt");
          }).catch(function(rejected) {
            console.log(rejected);
            alert("Die Datenbank konnte nicht erreicht werden");
            location.reload();
          });
        },
        function(form) {
          console.log(form);
          alert("Die Antwort konnte nicht erstellt werden");
        }, requestList, additionRequestInfo)

  }).catch(function(rejected) {
    console.log(rejected);
    alert("Die Datenbank konnte nicht erreicht werden.");
    location.reload();
  });
}

function editAnswer(answerId, defaultText, defaultSelection, node) {

  categoryRequest = {'request':'Category'};
  getRequest(categoryRequest).then(function(resolved) {
    var categoryList = [];

    for(var i=0; i<resolved.length; i++) {
      var option = {};
      option.Value = resolved[i].Id;
      option.Text = resolved[i].Title + " (" + resolved[i].Id + ")";
      categoryList[i] = option;
    }

    var textRequest = {};
    textRequest.Type = MODAL_TYPE_TEXT;
    textRequest.TextRepresentation = "Neuer Antworttext eingeben";
    textRequest.DefaultText = defaultText;

    var selectRequest = {};
    selectRequest.Type = MODAL_TYPE_SELE;
    selectRequest.Name = "categoryId";
    selectRequest.Options = categoryList;
    selectRequest.DefaultSelection = defaultSelection;

    var requestList = [textRequest, selectRequest];

    var additionRequest = {};
    additionRequest.Request = 'answerId';
    additionRequest.Value = answerId;
    var additionRequestInfo = [additionRequest];

    buildAddModal("Edit_Answer", "Antwort Bearbeiten",
        function(form) {
          return true;
        },
        function(form) {
          //edit the Answer
          setRelativeInformation(node, ANSWER_TEXT_CELL, form.get("text"));
          setRelativeInformation(node, ANSWER_CATEGORY_CELL, form.get(selectRequest.Name));
          alert("Die Antwort wurde bearbeitet");
        },
        function(form) {
          console.log(form);
          alert("Die Antwort konnte nicht bearbeitet werden");
        }, requestList, additionRequestInfo)

  }).catch(function(rejected) {
    console.log(rejected);
    alert("There was a problem getting the categories");
  });
}

//toggles between showing answers ans hiding answers
function toggleShowHide(calledFrom){
  if($(calledFrom).parent().parent().find( ".answers").height() == 0){
      $(calledFrom).css("transform","rotate(90deg)");
      $(calledFrom).parent().parent().find( ".answers" ).css( "max-height", "999px" );
  }
  else{
      $(calledFrom).css("transform","rotate(0deg)");
      $(calledFrom).parent().parent().find( ".answers" ).css( "max-height", "0px" );
  }
}
