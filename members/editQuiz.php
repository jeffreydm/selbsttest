<?php
declare(strict_types=1);
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

require_once '../php/includes/constants.php';
require_once '../php/classes/Membership.php';
require_once '../get_db_entries.php';

$membership = New Membership();
$membership->confirm_Member();

class EditConnectorSQL {
  private $conn;
  private $returnValue;

  function __construct() {
    $this->conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_SELBSTTEST) or
            die('There was a problem connecting to the database.');
    $this->conn->set_charset('utf8');
  }

  function newQuestion($text) : void {
    $stmt = $this->conn->prepare("INSERT INTO Question(Id, Text, Image) VALUES(NULL, ?, NULL)");
    $stmt->bind_param("s", $text);
    $this->returnValue = $stmt->execute();
  }

  function editQuestion($id, $text) : void {
    $stmt = $this->conn->prepare("UPDATE Question SET Text=? WHERE Id=?");
    $stmt->bind_param("ss", $text, $id);
    $this->returnValue = $stmt->execute();
  }

  function deleteQuestion($questionId) : void {
    $stmt = $this->conn->prepare( "DELETE FROM Question WHERE Id = ?" );
    $stmt->bind_param("s", $questionId);
    $this->returnValue = $stmt->execute();
  }

  function newAnswer($questionId, $categoryId, $text) : void {
    $stmt = $this->conn->prepare( "INSERT INTO Answer" .
    "(Id, QuestionId, CategoryId, Text, Image) VALUES (NULL, ?, ?, ?, NULL)" );
		$stmt->bind_param("sss", $questionId, $categoryId, $text);
		$this->returnValue = $stmt->execute();
  }

  function editAnswer($id, $text, $categoryId) : void {
    $stmt = $this->conn->prepare("UPDATE Answer SET Text=?, CategoryId=? WHERE Id=?");
    $stmt->bind_param("sss", $text, $categoryId, $id);
    $this->returnValue = $stmt->execute();
  }

  function deleteAnswer($answerId) : void {
    $stmt = $this->conn->prepare( "DELETE FROM Answer WHERE Id = ?" );
    $stmt->bind_param("s", $answerId);
    $this->returnValue = $stmt->execute();
  }

  function newCategory($name) : void {
    $getter = new ConnectorSQL();
    $getter->get_results();
    $results = $getter->getReturnValue();

    mysqli_begin_transaction($this->conn);

    try {
      $category_stmt = $this->conn->prepare( "INSERT INTO Category(Id, Title) VALUES (NULL, ?)" );
      $category_stmt->bind_param("s", $name);
      if( ! $category_stmt->execute() )
        throw new Exception("Category entry was not created");
      $categoryId = $this->conn->insert_id;

      $result_category_stmt = $this->conn->prepare( "INSERT INTO Result_Category" .
      "(ResultId, CategoryId, Minimum, Maximum) VALUES (?, ?, ?, ?)" );
      for($i=0; $i<count($results); $i++) {
        $zero = "0";
        $result_category_stmt->bind_param("ssss", $results[$i]['Id'], $categoryId, $zero, $zero);
        if( ! $result_category_stmt->execute())
          throw new Exception("Result -> Category entry was not created");
      }

      mysqli_commit($this->conn);
      $this->returnValue = true;
    } catch (Exception $e) {
      $this->conn->rollback();
      $this->returnValue = false;
    }
  }

  function editCategory($id, $text) {
    $stmt = $this->conn->prepare("UPDATE Category SET Title=? WHERE Id=?");
    $stmt->bind_param("ss", $text, $id);
    $this->returnValue = $stmt->execute();
  }

  function deleteCategory($categoryId) : void {
    mysqli_begin_transaction($this->conn);

    try {
      $category_stmt = $this->conn->prepare( "DELETE FROM Category WHERE Id=?" );
      $category_stmt->bind_param("s", $categoryId);
      if( ! $category_stmt->execute() )
        throw new Exception("Category entry was not deleted");

      $result_category_stmt = $this->conn->prepare( "DELETE FROM Result_Category WHERE CategoryId=?");
      $result_category_stmt->bind_param("s", $categoryId);
      if( ! $result_category_stmt->execute())
        throw new Exception("Result -> Category entry was not created");

      mysqli_commit($this->conn);
      $this->returnValue = true;
    } catch (Exception $e) {
      $this->conn->rollback();
      $this->returnValue = false;
    }
  }

  function newResult($text, $categoryMap) : void {
    $getter = new ConnectorSQL();
    $getter->getNumberOfCategories();
    $numCategories = $getter->getReturnValue();

    if(count($categoryMap) == $numCategories) {
      mysqli_begin_transaction($this->conn);

      try {
        $result_stmt = $this->conn->prepare( "INSERT INTO Result" .
        "(Id, Text, Image) VALUES (NULL, ?, NULL)" );
        $result_stmt->bind_param("s", $text);
        if( ! $result_stmt->execute() )
          throw new Exception("Result entry was not created");
        $resultId = $this->conn->insert_id;

        $category_stmt = $this->conn->prepare( "INSERT INTO Result_Category" .
        "(ResultId, CategoryId, Minimum, Maximum) VALUES (?, ?, ?, ?)" );
        for($i=0; $i<count($categoryMap); $i++) {
          $element = (array) $categoryMap[ array_keys($categoryMap)[$i] ];
          $min = $element['min'];
          $max = $element['max'];
          $category_stmt->bind_param("ssss", $resultId, array_keys($categoryMap)[$i], $min, $max);
          if( ! $category_stmt->execute())
            throw new Exception("Result -> Category entry was not created");
        }

        mysqli_commit($this->conn);
        $this->returnValue = true;
      } catch (Exception $e) {
        $this->conn->rollback();
        $this->returnValue = false;
      }
    } else {
      $this->returnValue = false;
    }
  }

  function editResult($id, $text, $categoryMap) : void {
    mysqli_begin_transaction($this->conn);

    try {
      $result_stmt = $this->conn->prepare( "UPDATE Result SET Text=? WHERE Id=?" );
      $result_stmt->bind_param("ss", $text, $id);
      if( ! $result_stmt->execute() )
        throw new Exception("Result entry was not edited");

      $category_stmt = $this->conn->prepare( "UPDATE Result_Category SET Minimum=?, Maximum=? WHERE ResultId=? AND CategoryId=?" );
      for($i=0; $i<count($categoryMap); $i++) {
        $element = (array) $categoryMap[ array_keys($categoryMap)[$i] ];
        $min = $element['min'];
        $max = $element['max'];
        $category_stmt->bind_param("ssss", $min, $max, $id, array_keys($categoryMap)[$i]);
        if( ! $category_stmt->execute())
          throw new Exception("Result -> Category entry was not edited");
      }

      mysqli_commit($this->conn);
      $this->returnValue = true;
    } catch (Exception $e) {
      $this->conn->rollback();
      $this->returnValue = false;
    }
  }

  function deleteResult($resultId) : void {
    mysqli_begin_transaction($this->conn);

    try {
      $result_stmt = $this->conn->prepare( "DELETE FROM Result WHERE Id=?" );
      $result_stmt->bind_param("s", $resultId);
      if( ! $result_stmt->execute() )
        throw new Exception("Result entry was not deleted");

      $result_category_stmt = $this->conn->prepare( "DELETE FROM Result_Category WHERE ResultId=?");
      $result_category_stmt->bind_param("s", $resultId);
      if( ! $result_category_stmt->execute())
        throw new Exception("Result -> Category entry was not deleted");

      mysqli_commit($this->conn);
      $this->returnValue = true;
    } catch (Exception $e) {
      $this->conn->rollback();
      $this->returnValue = false;
    }
  }

  function getReturnValue() {
    $return = $this->returnValue;
    $this->returnValue = null;
    return $return;
  }

  function editTitle($change) : void{
    $stmt = $this->conn->prepare("UPDATE Quiz SET Title=? WHERE Id=1");
    $stmt->bind_param("s", $change);
    $this->returnValue = $stmt->execute();
  }

  function editDescription($change) : void{
    $stmt = $this->conn->prepare("UPDATE Quiz SET Description=? WHERE Id=1");
    $stmt->bind_param("s", $change);
    $this->returnValue = $stmt->execute();
  }

  function editButtonTitle($change) : void{
    $stmt = $this->conn->prepare("UPDATE Quiz SET ButtonText=? WHERE Id=1");
    $stmt->bind_param("s", $change);
    $this->returnValue = $stmt->execute();
  }

  function editButtonLink($change) : void{
    $stmt = $this->conn->prepare("UPDATE Quiz SET ButtonLink=? WHERE Id=1");
    $stmt->bind_param("s", $change);
    $this->returnValue = $stmt->execute();
  }
}

if ($_POST) {
  $connector = new EditConnectorSQL();

  switch ($_POST['request']) {

    case 'Add_Question':
      $connector->newQuestion($_POST['text']);
      break;

    case 'Edit_Question':
      $connector->editQuestion($_POST['questionId'], $_POST['text']);
      break;

    case 'Delete_Question':
      $connector->deleteQuestion($_POST['id']);
      break;

    case 'Add_Answer':
      $connector->newAnswer($_POST['questionId'], $_POST['categoryId'], $_POST['text']);
      break;

    case 'Edit_Answer':
      $connector->editAnswer($_POST['answerId'], $_POST['text'], $_POST['categoryId']);
      break;

    case 'Delete_Answer':
      $connector->deleteAnswer($_POST['id']);
      break;

    case 'Add_Category':
      $connector->newCategory($_POST['text']);
      break;

    case 'Edit_Category':
      $connector->editCategory($_POST['categoryId'], $_POST['text']);
      break;

    case 'Delete_Category':
      $connector->deleteCategory($_POST['id']);
      break;

    case 'Add_Result':
      $connector->newResult($_POST['text'], (array) json_decode($_POST['categoryMap']) );
      break;

    case 'Edit_Result':
      $connector->editResult($_POST['resultId'], $_POST['text'], (array) json_decode($_POST['categoryMap']) );
      break;

    case 'Delete_Result':
      $connector->deleteResult($_POST['resultId']);
      break;

    case 'editTitle':
      $connector->editTitle($_POST['text']);
      break;

    case 'editDescription':
      $connector->editDescription($_POST['text']);
      break;

    case 'editButtonTitle':
      $connector->editButtonTitle($_POST['text']);
      break;

    case 'editButtonLink':
      $connector->editButtonLink($_POST['text']);
      break;

    default:
      break;

    }
  echo json_encode( $connector->getReturnValue() );
}

?>
