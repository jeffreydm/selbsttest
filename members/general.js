const LABEL_CELL = 0;
const VALUE_CELL = 1;
const EDIT_BUTTON_CELL = 2;


start();


function start() {
  dataMap = {'request':'General'};
  getRequest(dataMap).then(function(resolved) {
    setUp(resolved);
  }).catch(function(rejected) {
    console.log(rejected);
    alert("There was a problem setting up");
  });
}

function setUp(general) {
  var tableBody = document.getElementById("question_table_body");

  var rowTitle = tableBody.insertRow();

  var labelCellTitle = rowTitle.insertCell(LABEL_CELL);
  $(labelCellTitle).addClass("labelCell");
  var labelValTitle = document.createTextNode("Quiz Titel");
  labelCellTitle.appendChild(labelValTitle);

  var valueCellTitle = rowTitle.insertCell(VALUE_CELL);
  $(valueCellTitle).addClass("valueCell");
  valueCellTitle.setAttribute("data-val", general.Title);
  var valueValTitle = document.createTextNode(general.Title);
  valueCellTitle.appendChild(valueValTitle);

  //edit button start
  var editCellTitle = rowTitle.insertCell(EDIT_BUTTON_CELL);
  $(editCellTitle).addClass("editButtonCellQuizTitel");
  var editButtonQuizTitle = document.createElement("Button");
  $(editButtonQuizTitle).addClass("editButtonQuizTitel");
  editButtonQuizTitle.addEventListener("click", function() {
    editTitle(editButtonQuizTitle);
  });
  editCellTitle.appendChild(editButtonQuizTitle);
  //edit button end


  rowDescription = tableBody.insertRow();

  labelCellDescription = rowDescription.insertCell(LABEL_CELL);
  $(labelCellDescription).addClass("labelCell");
  labelValDescription = document.createTextNode("Beschreibung");
  labelCellDescription.appendChild(labelValDescription);

  valueCellDescription = rowDescription.insertCell(VALUE_CELL);
  $(valueCellDescription).addClass("valueCell");
  valueCellDescription.setAttribute("data-val", general.Description);
  valueVal = document.createTextNode(general.Description);
  valueCellDescription.appendChild(valueVal);

  //edit button start
  editCellDescription = rowDescription.insertCell(EDIT_BUTTON_CELL);
  $(editCellDescription).addClass("editButtonCellQuizDescription");
  editButtonDescription = document.createElement("Button");
  $(editButtonDescription).addClass("editButtonQuizDescription");
  editButtonDescription.addEventListener("click", function() {
    editDescription(editButtonDescription);
  });
  editCellDescription.appendChild(editButtonDescription);
  //edit button end


  rowButtonText = tableBody.insertRow();

  labelCellButtonText = rowButtonText.insertCell(LABEL_CELL);
  $(labelCellButtonText).addClass("labelCell");
  labelValButtonText = document.createTextNode("Endscreen Weiterleitungsknopf Titel");
  labelCellButtonText.appendChild(labelValButtonText);

  valueCellButtonText = rowButtonText.insertCell(VALUE_CELL);
  $(valueCellButtonText).addClass("valueCell");
  valueCellButtonText.setAttribute("data-val", general.ButtonText);
  valueVal = document.createTextNode(general.ButtonText);
  valueCellButtonText.appendChild(valueVal);

  //edit button start
  editCellButtonText = rowButtonText.insertCell(EDIT_BUTTON_CELL);
  $(editCellButtonText).addClass("editButtonCellEndscreenButtonTitel");
  editButtonButtonText = document.createElement("Button");
  $(editButtonButtonText).addClass("editButtonEndscreenButtonTitel");
  editCellButtonText.appendChild(editButtonButtonText);
  editButtonButtonText.addEventListener("click", function() {
    editButtonTitle(editButtonButtonText);
  });
  //edit button end


  rowButtonLink = tableBody.insertRow();

  labelCellButtonLink = rowButtonLink.insertCell(LABEL_CELL);
  $(labelCellButtonLink).addClass("labelCell");
  labelValButtonLink = document.createTextNode("Endscreen Weiterleitungsknopf Link");
  labelCellButtonLink.appendChild(labelValButtonLink);

  valueCellButtonLink = rowButtonLink.insertCell(VALUE_CELL);
  $(valueCellButtonLink).addClass("valueCell");
  valueCellButtonLink.setAttribute("data-val", general.ButtonLink);
  valueVal = document.createTextNode(general.ButtonLink);
  valueCellButtonLink.appendChild(valueVal);

  //edit button start
  editCellButtonLink = rowButtonLink.insertCell(EDIT_BUTTON_CELL);
  $(editCellButtonLink).addClass("editButtonCellEndscreenButtonLink");
  editButtonButtonLink = document.createElement("Button");
  $(editButtonButtonLink).addClass("editButtonEndscreenButtonLink");
  editCellButtonLink.appendChild(editButtonButtonLink);
  editButtonButtonLink.addEventListener("click", function () {
    editButtonLink(editButtonButtonLink);
  });
    //edit button end
}

function editTitle(node) {
  var modalTitleVal = "Quiz Titel";
  var requestType = "editTitle";

  var defaultText = getRelativeInformation(node, VALUE_CELL);

  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neuen Text eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  buildAddModal(requestType, modalTitleVal,
    function(form) {
      //do nothing
      return true;
    },
    function(form) {
      setRelativeInformation(node, VALUE_CELL, form.get("text"));
      alert("Der Titel wurde bearbeitet");
    },
    function(form) {
      console.log(form);
      alert("ERROR: Der Titel konnte nicht bearbeitet werden");
    }, requestList, []
  );
}
function editDescription(node) {
  var modalTitleVal = "Beschreibung";
  var requestType = "editDescription";

  var defaultText = getRelativeInformation(node, VALUE_CELL);

  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neuen Text eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  buildAddModal(requestType, modalTitleVal,
    function(form) {
      //do nothing
      return true;
    },
    function(form) {
      setRelativeInformation(node, VALUE_CELL, form.get("text"));
      alert("Die Beschreibung wurde bearbeitet");
    },
    function(form) {
      console.log(form);
      alert("ERROR: Der Titel konnte nicht bearbeitet werden");
    }, requestList, []
  );
}
function editButtonTitle(node) {
  var modalTitleVal = "Endscreen Weiterleitungsknopf Titel";
  var requestType = "editButtonTitle";

  var defaultText = getRelativeInformation(node, VALUE_CELL);

  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neuen Text eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  buildAddModal(requestType, modalTitleVal,
    function(form) {
      //do nothing
      return true;
    },
    function(form) {
      setRelativeInformation(node, VALUE_CELL, form.get("text"));
      alert("Der Endscreen Knopf Text wurde bearbeitet");
    },
    function(form) {
      console.log(form);
      alert("ERROR: Der Endscreen Knopf Text konnte nicht bearbeitet werden");
    }, requestList, []
  );
}
function editButtonLink(node) {
  var modalTitleVal = "Endscreen Weiterleitungsknopf Link";
  var requestType = "editButtonLink";

  var defaultText = getRelativeInformation(node, VALUE_CELL);

  var request = {};
  request.Type = "text";
  request.TextRepresentation = "Neuen Text eingeben";
  request.DefaultText = defaultText;
  var requestList = [request];

  buildAddModal(requestType, modalTitleVal,
    function(form) {
      //do nothing
      return true;
    },
    function(form) {
      setRelativeInformation(node, VALUE_CELL, form.get("text"));
      alert("Der Endscreen Knopf Link wurde bearbeitet");
    },
    function(form) {
      console.log(form);
      alert("ERROR: Der Endscreen Knopf Link konnte nicht bearbeitet werden");
    }, requestList, []
  );
}
