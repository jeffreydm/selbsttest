<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

require_once 'php/includes/constants.php';

class ConnectorSQL {
  private $conn;
  private $returnValue;

  function __construct() {
    $this->conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_SELBSTTEST) or
            die('There was a problem connecting to the database.');
    $this->conn->set_charset('utf8');
  }

  //returns the descript of the quiz
  function getButtonText() {
    $bt_query = "SELECT ButtonText FROM `Quiz` WHERE 1";
    $bt_ButtonText = $this->conn->query($bt_query);

    if($bt_ButtonText) {
      $bt_text = $bt_ButtonText->fetch_assoc();
      $this->returnValue = $bt_text;
    } else {
      $this->returnValue = "could not read db";
    }
  }

  //returns the descript of the quiz
  function getButtonLink() {
    $bl_query = "SELECT ButtonLink FROM `Quiz` WHERE 1";
    $bl_ButtonLink = $this->conn->query($bl_query);

    if($bl_ButtonLink) {
      $bl_text = $bl_ButtonLink->fetch_assoc();
      $this->returnValue = $bl_text;
    } else {
      $this->returnValue = "could not read db";
    }
  }

  //returns the descript of the quiz
  function get_description() {
    $d_query = "SELECT Description FROM `Quiz` WHERE 1";
    $d_description = $this->conn->query($d_query);

    if($d_description) {
      $d_text = $d_description->fetch_assoc();
      $this->returnValue = $d_text;
    } else {
      $this->returnValue = "could not read db";
    }
  }


  //return all question information (id, text, answers: (id, text, categoryid))
  function get_questions() {
    $q_query = "SELECT *
        FROM Question";
    $q_result = $this->conn->query($q_query);

    $q_list = array();
    while($q_row = $q_result->fetch_assoc()) {

      $a_query = "SELECT *
          FROM Answer
          WHERE QuestionId=" . $q_row["Id"];
      $a_result = $this->conn->query($a_query);

      $a_list = array();
      while($a_row = $a_result->fetch_assoc()) {
        $a_object = array('Id' => $a_row['Id'], 'Text' => $a_row['Text'], 'CategoryId' => $a_row['CategoryId']);
        array_push($a_list, $a_object);
      }

      $q_object = array('Id' => $q_row['Id'], 'Text' => $q_row['Text'], 'Answers' => $a_list);
      array_push($q_list, $q_object);
    }

    $this->returnValue = $q_list;
  }

  //return all category information (id, title)
  function get_categories() {
    $c_query = "SELECT *
        FROM Category";
    $c_result = $this->conn->query($c_query);

    $c_list = array();
    while($c_row = $c_result->fetch_assoc()) {
      $c_object = array('Id' => $c_row['Id'], 'Title' => $c_row['Title']);
      array_push($c_list, $c_object);
    }

    $this->returnValue = $c_list;
  }

  //return all result information (id, quizid, text)
  function get_results() {
    $r_query = "SELECT *
        FROM Result";
    $r_result = $this->conn->query($r_query);

    $r_list = array();
    while($r_row = $r_result->fetch_assoc()) {
      $r_object = array('Id' => $r_row['Id'], 'Text' => $r_row['Text']);
      array_push($r_list, $r_object);
    }

    $this->returnValue = $r_list;
  }

  /* A Result ID becomes a list of Categories (with all values related to the Category: Min, Max and ID)
   * The Result ID is then packed into a list under "ResultId" with its Category informtion being added under "CategoryInfo"
   * > this is for the sake of the JS side of things
   */
  function get_result_to_categories() {
    //get all entries in the db
    $r2c_query = "SELECT *
        FROM Result_Category";
    $r2c_result = $this->conn->query($r2c_query);

    $r2c_list = array();
    while($r2c_row = $r2c_result->fetch_assoc()) {
      //save the ResultId in $res
      $res = $r2c_row['ResultId'];

      //if this ResultId has not already been seen
      if(!array_key_exists($res, $r2c_list)) {
        //create an array entry for it
        $r2c_list[$res] = array();
      }
      //create an array of all the relevant Category Infomation
      $category_array = array('CategoryId' => $r2c_row['CategoryId'], 'Minimum' => $r2c_row['Minimum'], 'Maximum' => $r2c_row['Maximum']);
      //push the Category Information into the array connected to that ResultId
      array_push($r2c_list[$res], $category_array);
    }

    //make the array easier to use
    $r2c_modded = array();
    foreach ($r2c_list as $key => $value) {
      $r2c_temp = array();
      $r2c_temp['ResultId'] = $key;
      $r2c_temp['CategoryInfo'] = $value;
      array_push($r2c_modded, $r2c_temp);
    }

    $this->returnValue = $r2c_modded;
  }

  //get the result test for a specific result id
  function get_result_text($id) {
    $stmt = $this->conn->prepare("SELECT * FROM Result WHERE Id = ?");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$rt_result = $stmt->get_result();

    $this->returnValue = $rt_result->fetch_assoc()['Text'];
  }

  function getNumberOfQuestions() {
    $query = "SELECT COUNT(*) FROM `Question`";
    $result = $this->conn->query($query);
    $num = intval( ($result->fetch_assoc())["COUNT(*)"] );
    $this->returnValue = $num;
  }

  function getNumberOfCategories() {
    $query = "SELECT COUNT(*) FROM `Category`";
    $result = $this->conn->query($query);
    $num = intval( ($result->fetch_assoc())["COUNT(*)"] );
    $this->returnValue = $num;
  }

  function getGeneralFields() {
    $stmt = $this->conn->prepare("SELECT * FROM Quiz WHERE Id = 1");
    $stmt->execute();
    $result = $stmt->get_result();

    $this->returnValue = $result->fetch_assoc();
  }

  //returns the value that the previous method created
  function getReturnValue() {
    return $this->returnValue;
  }

  function getLastId($table) {
    switch($table) {
      case 'Question':
        $stmt = $this->conn->prepare("SELECT MAX(Id) FROM Question");
        break;
      
      case 'Answer':
        $stmt = $this->conn->prepare("SELECT MAX(Id) FROM Answer");
        break;
      
      case 'Result':
        $stmt = $this->conn->prepare("SELECT MAX(Id) FROM Result");
        break;
      
      case 'Category':
        $stmt = $this->conn->prepare("SELECT MAX(Id) FROM Category");
        break;
      
      default:
        throw new Exception("Bad table request");
        break;
    }
    $stmt->execute();
    $result = $stmt->get_result();

    $this->returnValue = $result->fetch_assoc();
  }
}

//figures our what kind of get request we received and returns the requested information
if($_GET) {
  $connector = new ConnectorSQL();
  switch ($_GET['request']) {

    case 'ButtonLink':
      $connector->getButtonLink();
      break;

    case 'ButtonText':
      $connector->getButtonText();
      break;

    case 'Description':
      $connector->get_description();
      break;

    case 'Question':
      $connector->get_questions();
      break;

    case 'Category':
      $connector->get_categories();
      break;

    case 'Result':
      $connector->get_results();
      break;

    case 'Result_Category':
      $connector->get_result_to_categories();
      break;

    case 'Result_Text':
      $connector->get_result_text($_GET['id']);
      break;

    case 'Question_Total':
      $connector->getNumberOfQuestions();
      break;

    case 'General':
      $connector->getGeneralFields();
      break;
    
    case 'Last_Id':
      $connector->getLastId($_GET['table']);
      break;

    default:
      "Bad Get Request";
      break;
  }

  $returnVal = $connector->getReturnValue();
  if($returnVal != null)
    echo json_encode($returnVal);
}

?>
