<?php
declare(strict_types=1);

class Range {
  private $min;
  private $max;

  public function __construct($newMin, $newMax) {
    if( $this->checkValid($newMin, $newMax) ) {
      $this->min = $newMin;
      $this->max = $newMax;
    } else {
      throw new InvalidArgumentException();
    }
  }

  private function checkValid($min, $max) : bool {
    if( is_int($min) && is_int($max) &&
          0<=$min && 0<=$max &&
          $min<=$max )
      return true;
    return false;
  }

  public function getMin() : int {
    return $this->min;
  }
  public function getMax() : int {
    return $this->max;
  }
  public function setMin($newMin) : void {
    if( $this->checkValid($newMin, $this->max) ) {
      $this->min = $newMin;
    } else {
      throw new InvalidArgumentException();
    }
  }
  public function setMax($newMax) : void {
    if( $this->checkValid($this->min, $newMax) ) {
      $this->max = $newMax;
    } else {
      throw new InvalidArgumentException();
    }
  }
}

?>
