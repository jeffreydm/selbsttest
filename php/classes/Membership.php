<?php

require 'MySQL.php';

class Membership {

	function validate_User($username, $password) {
		$mysql = New Mysql();
		$ensure_credentials = $mysql->verify_Username_and_Pass($username, $password);

		if($ensure_credentials) {
			$_SESSION['status'] = 'authorized';
			header("location: /members");
		} else return "Please enter a correct username and password";

	}

	function log_User_Out() {
		if(isset($_SESSION['status'])) {
			unset($_SESSION['status']);
		}
	}

	function confirm_Member() {
		session_start();
		if($_SESSION['status'] !='authorized') header("location: /login");
	}

}

?>
