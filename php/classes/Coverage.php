<?php
declare(strict_types=1);

require_once __DIR__ . '/' . 'Range.php';

class Coverage {

  private $fillage;

  public function __construct() {
  	$this->fillage = array();
  }

  public function isThereACollision($check) : bool {
    if($check instanceof Range) {
      $retVal = false;
      foreach($this->fillage as $range) {
        if( $this->isThereARangeCollision($range, $check) )
          $retVal = true;
      }
      return $retVal;
    } else {
      throw new InvalidArgumentException();
    }
  }
  public function isThereARangeCollision($range, $check) : bool {
    if( $range->getMin()<=$check->getMin() && $check->getMin()<=$range->getMax() ) {
      return true;
    } elseif ( $check->getMin()<$range->getMin() && $check->getMax()>=$range->getMin() ) {
      return true;
    } else {
      return false;
    }
  }

  public function add($newRange) : void {
  	$conglamorate = $newRange;
    foreach($this->fillage as $key => $range) {
      try {
        $concatted = $this->concat($range, $conglamorate);
        unset($this->fillage[$key]);
        $conglamorate = $concatted;
      } catch(InvalidArgumentException $e) {
        //do nothing
      }
    }
    array_push($this->fillage, $conglamorate);
  }
  private function concat($range1, $range2) : Range {
  	if( $range1->getMin()>$range2->getMax() && $range1->getMin()-$range2->getMax() <= 1 ) {
      return new Range($range2->getMin(), $range1->getMax());
    } elseif( $range2->getMin()>$range1->getMax() && $range2->getMin()-$range1->getMax() <= 1 ) {
      return new Range($range1->getMin(), $range2->getMax());
    } elseif( $range1->getMin()>=$range2->getMin() && $range1->getMin()<=$range2->getMax() ) {
      if($range1->getMax()<$range2->getMax()) {
      	  return new Range($range2->getMin(), $range2->getMax());
        } else {
          return new Range($range2->getMin(), $range1->getMax());
        }
    } elseif( $range2->getMin()>=$range1->getMin() && $range2->getMin()<=$range1->getMax() ) {
      if($range2->getMax()<$range1->getMax()) {
      	  return new Range($range1->getMin(), $range1->getMax());
        } else {
          return new Range($range1->getMin(), $range2->getMax());
        }
    } else {
      throw new InvalidArgumentException();
    }
  }

  public function remove($newRange) : void {
    foreach($this->fillage as $key => $range) {
      //if the new range is in contact with any existing range
      if($this->isThereACollision($range, $newRange)) {
        if( ! $this->shave($key, $range, $newRange) )
          /*this isn't good*/;
      }
    }
  }
  private function shave($key, $range, $toRemove) : bool {
    //if we want to remove from beginning to end
    if( $toRemove->getMin()<=$range->getMin() && $range->getMax()<=$toRemove->getMax() ) {
      //remove the entire range
      unset($this->fillage[$key]);
      return true;
    }
    //if we want to remove from the beginning
    elseif( $toRemove->getMin()<=$range->getMin() && $toRemove->getMax()<$range->getMax() ) {
      unset($this->fillage[$key]);
      array_push($this->fillage, new Range($toRemove->getMax()+1, $range->getMax()));
      return true;
    }
    //if we want to remove from the middle
    elseif( $range->getMin()<$toRemove->getMin() && $toRemove->getMax()<$range->getMax() ) {
      unset($this->fillage[$key]);
      array_push($this->fillage, new Range($range->getMin(), $toRemove->getMin()-1));
      array_push($this->fillage, new Range($toRemove->getMax()+1, $range->getMax()));
      return true;
    }
    //if we want to remove from the end
  	elseif( $range->getMin()<$toRemove->getMin() && $toRemove->getMin()<=$range->getMax() ) {
      unset($this->fillage[$key]);
      array_push($this->fillage, new Range($range->getMin(), $toRemove->getMin()-1));
      return true;
    }
    //if we didn't remove anything
    return false;
  }

  public function isEmpty() : bool {
    if(empty($this->fillage)) {
      return true;
    } else {
      return false;
    }
  }
  public function getFillage() : bool {
    return $this->fillage;
  }
}

?>
