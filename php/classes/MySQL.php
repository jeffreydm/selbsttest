<?php

require_once __DIR__ . '/../' . '/includes/constants.php';

class Mysql {
	private $conn;

	function __construct() {
		$this->conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_MEMBERS) or
					  die('There was a problem connecting to the database.');
    	$this->conn->set_charset('utf8');
	}

	function verify_Username_and_Pass($username, $password) {
		$stmt = $this->conn->prepare("SELECT password FROM users WHERE username = ? LIMIT 1");
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$result = $stmt->get_result();

		/*
		$query = "SELECT password FROM users WHERE " .
							"username = \"" . $escapedUsername . "\" LIMIT 1";
		$result = $this->conn->query($query);
		*/

		if($result) {
			$savedPasswordHash = rtrim($result->fetch_assoc()['password']);
			if(password_verify($password, $savedPasswordHash)) {
				return true;
			}
		}
		return false;
	}
}

?>
