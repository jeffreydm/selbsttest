<?php

declare(strict_types=1);

class Algorithm {

  //generate all combinations for a total: 4 -> 4, 31, 22, 211, 1111
  public static function partition($n) : array {
    //if a negative number is passed, discontinue
    if($n<0) {
      throw new InvalidArgumentException;
    }
    //if the array is empty
    if($n<1) {
      return array();
    }
    //create the return array
    $retVal = array();
    //add an array containing only n to the return array
    $starter = array();
    array_push($starter, $n);
    array_push($retVal, $starter);
    //if n is larger than 1, then combine all subpartitions
    if($n>1) {
      for($i=1; $i<$n; $i++) {
        $returned = Algorithm::partition($i);

        for($j=0; $j<count($returned); $j++) {
          array_unshift($returned[$j], $n-$i);
          if(max($returned[$j])<=$n-$i)
            array_push($retVal, $returned[$j]);
        }
      }
    }
    return $retVal;
  }

  //generate all combinations of a set: 123, 132, 213, 231, 312, 321
  public static function permeate($array) : array {
    //if it is not an array
    if( !is_array($array) ) {
      throw new InvalidArgumentException("Not an array");
    }
    //if the array is empty
    if (count($array)<1) {
      throw new InvalidArgumentException("Array empty");
    }

    //create an array where we will store all permeations
    $returnArray = array();
    //if the array has exactly one element
    if (count($array) == 1) {
      //then simply return the array we received (packaged properly)
      array_push($returnArray, $array);
    } else {
      //otherwise, work through the array creating every combination possible

      //for every element in the array we were given
      for($i=0; $i<count($array); $i++) {
        //temporary array we can mess around with
        $addArray = $array;
        //remove one of the elements
        array_splice($addArray, $i, 1);
        //get all permeations for the array without said element
        $allPermeations = Algorithm::permeate($addArray);
        //then for each of those combinations
        foreach($allPermeations as $permeations) {
          //add the removed element back to the front
          array_push($permeations, $array[$i]);
          //and add it to the list of combinations
          array_push($returnArray, $permeations);
        }
      }
    }

    return $returnArray;
  }
}

?>
