<?php
declare(strict_types=1);

require_once __DIR__ . '/../../' . 'get_db_entries.php';
require_once __DIR__ . '/' . 'Algorithm.php';
require_once __DIR__ . '/' . 'Coverage.php';

/**
 * checks if the test covers every possibility without any conflicts
 * Result Array structure:
 * Base (array)         : [ Result, Result... ]
 * Result (map)         : [ "ResultId" => # , "CategoryInfo" => CategoryInfo ]
 * CategoryInfo (array) : [ Category, Category, Category ]
 * Category (map)       : [ "CategoryId" => #, "Minimum" => #, "Maximum" => # ]
 */
class Validity {
  //check if a combination is covered by multiple results
  function checkForConflicts($results, $numQuestions, $numCategories) : string {
    $allCombinations = $this->getAllCombinations($results, $numQuestions, $numCategories);
    $duplicateValues = $this->array_not_unique($allCombinations);

    //if there were duplicate values, then we have a conflict
    if(count($duplicateValues)>0) {
      $duplicateList = "";
      foreach($this->array_unique_multidimensional($duplicateValues) as $duplicate) {
        $duplicateList .= "[";
        foreach($duplicate as $element) {
          $duplicateList .= $element . ",";
        }
        $duplicateList .= "] ";
      }
      return "ERROR: Es gibt Fälle die zu mehr als einem Ergebnis führen können.\n" . $duplicateList . "\n\n";
    } else {
      //if nothing went wrong, return a positive
      return "Gut: Es gibt keine Fälle die mehr als einem Ergebnis zugewiesen sind.\n\n";
    }
  }

  //check if any combinations of answers are not covered
  function checkForGaps($resultArray, $numQuestions, $numCategories) : string {
    if($numQuestions<1)
      throw new LengthException("Not enough questions");
    if($numCategories<1)
      throw new LengthException("Not enough categories");

    //create an array with every possible combination (based on our number of questions)
    $allCombinations = array();

    //create a set with every possible number combination, for example:
      //(4,0,0),(3,1,0),(2,2,0),(2,1,1)
        //filter out ones that have more parts than we have categories (1,1,1,1) -> x
        //and add 0s to the ends of ones that are too small (4) -> (4,0,0)
    $partitions = array();
    foreach ( Algorithm::partition($numQuestions) as $array ) {
      if( count($array) <= $numCategories ) {
        if( count($array) < $numCategories ) {
          while( count($array) < $numCategories ) {
            array_push($array, 0);
          }
        }
        array_push($partitions, $array);
      }
    }
    //for each of these sets create every possible combination, for example:
      //(4,0,0) -> (4,0,0), (0,4,0), (0,0,4)
    foreach ( $partitions as $partition ) {
      $permeations = $this->array_unique_multidimensional(Algorithm::permeate($partition));
      foreach ( $permeations as $permeation ) {
        array_push($allCombinations, $permeation);
      }
    }

    //for every Result entry, remove the combinations represented from the array
    $allRemoveArrays = $this->array_unique_multidimensional($this->getAllCombinations($resultArray, $numQuestions, $numCategories));
    
    sort($allCombinations);
    sort($allRemoveArrays);

    //get the values that are in both
    $inBoth = array_uintersect($allCombinations, $allRemoveArrays, array($this, 'compareArrayValue'));

    $allContained = count( $inBoth ) == count($allCombinations);
    //if there are any entries left, that combination is not covered by our test

    if( $allContained ) {
      return "Gut: Es gibt keine Fälle die keinem Ergebnis zugewiesen sind.\n\n";
    } else {
      $missing = array_udiff($allCombinations, $allRemoveArrays, array($this, 'compareArrayValue'));
      $missingList = "";
      foreach($missing as $missed) {
        $missingList .= "[";
        foreach($missed as $element) {
          $missingList .= $element . ",";
        }
        $missingList .= "] ";
      }
      return "ERROR: Es gibt Fälle die keinem Ergebnis zugewiesen sind.\n" . $missingList . "\n\n";
    }
  }

  //check if each result is possible
  function checkEachResult($results, $numQuestions) {
    foreach($results as $result) {
      $minTotal = 0;
      $maxTotal = 0;
      foreach($result["CategoryInfo"] as $category) {
        //TODO check exists?
        $minTotal += $category["Minimum"];
        $maxTotal += $category["Maximum"];
      }
      if($minTotal>$maxTotal || $minTotal>$numQuestions || $maxTotal<$numQuestions )
        return $result["ResultId"];
    }
    return -1;
  }

  function getAllCombinations($results, $numQuestions, $numCategories) {
    $allRemoveArrays = array();
    foreach ( $results as $result ) {
      $removeArrays = array();
      $categories = $result["CategoryInfo"];

      //since there must be at least 1 category, instantiate our array with values from Min to Max
      for ( $i = 0; $i <= ($categories[0]["Maximum"] - $categories[0]["Minimum"]); $i++ ) {
        $newArray = array();
        array_push($newArray, $categories[0]["Minimum"]+$i);
        array_push($removeArrays, $newArray);
      }

      //now add to our instantiated values with the values in the following categories
      for ( $i = 1; $i < $numCategories; $i++ ) {
        //create an empty array that will replace our previous removeArrays
        $newRemoveArrays = array();

        //get the min and max
        $min = $categories[$i]["Minimum"];
        $max = $categories[$i]["Maximum"];

        //for every number from min to max, add to our array and replace the previous values
        for ( $j = 0; $j <= $max - $min; $j++ ) {
          foreach ( $removeArrays as $prevArray ) {
            $newArray = $prevArray;
            array_push($newArray, $min+$j);
            array_push($newRemoveArrays, $newArray);
          }
        }

        //replace the previous removeArrays with our new improved list
        $removeArrays = $newRemoveArrays;
      }

      foreach($removeArrays as $array) {
        if(array_sum($array)==$numQuestions)
          array_push($allRemoveArrays, $array);
      }
    }

    return $allRemoveArrays;
  }

  function array_unique_multidimensional($input) {
    $serialized = array_map('serialize', $input);
    $unique = array_unique($serialized);
    return array_intersect_key($input, $unique);
  }

  function compareArrayValue($val1, $val2) {
    for($i=0; $i<count($val1); $i++) {
      if( $val1[$i] != $val2[$i] )
        return -1;
    }
    return 0;
  }

  function array_not_unique($array) {
    return array_diff_key($array, array_unique($array, SORT_REGULAR));
  }
}

if($_GET) {
  //get necessary info
  $connector = new ConnectorSQL();

  $connector->get_result_to_categories();
  $results = $connector->getReturnValue();

  $connector->get_categories();
  $categories = $connector->getReturnValue();

  $connector->getNumberOfQuestions();
  $numQuestions = $connector->getReturnValue();

  $connector->getNumberOfCategories();
  $numCategories = $connector->getReturnValue();

  $connector = null;
  
  $returnValue = "Ergebnis Abdeckung:\n\n";
  
  //check the validity
  $val = new Validity();
  if($numQuestions<1) {
    $returnValue .= "Keine Fragen vorhanden. Konnte nicht geprüft werden.";
  } elseif($numCategories<1) {
    $returnValue .= "Keine Kategorien vorhanden. Konnte nicht geprüft werden.";
  } else {
    $indCheckVal = $val->checkEachResult($results, $numQuestions);
    if($indCheckVal == -1) {
      $returnValue .= $val->checkForGaps($results, $numQuestions, $numCategories);
      $returnValue .= $val->checkForConflicts($results, $numQuestions, $numCategories);
    } else {
      $returnValue .= "Das Ergebnis " . $indCheckVal . " ist nicht möglich. Bitte überprüfen sie das Ergebnis und probieren es dann nochmal.";
    }
  }
  echo json_encode($returnValue);
}

?>
