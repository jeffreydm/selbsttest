<?php
require_once __DIR__ . '/../' . 'classes/MathStuff.php';

use PHPUnit\Framework\TestCase;

class MathStuffTest extends TestCase {

  public function permeateListProvider() {
    return [
      //give it
      [
        [1,2,3],
        [
          [3,2,1],
          [3,1,2],
          [2,3,1],
          [2,1,3],
          [1,3,2],
          [1,2,3]
        ]
      ],
      //expected
      [
        [1,2,3,4],
        [
          [4,3,2,1],
          [4,3,1,2],
          [4,2,3,1],
          [4,2,1,3],
          [4,1,3,2],
          [4,1,2,3],
          [3,4,2,1],
          [3,4,1,2],
          [3,2,4,1],
          [3,2,1,4],
          [3,1,4,2],
          [3,1,2,4],
          [2,4,3,1],
          [2,4,1,3],
          [2,3,4,1],
          [2,3,1,4],
          [2,1,4,3],
          [2,1,3,4],
          [1,4,3,2],
          [1,4,2,3],
          [1,3,4,2],
          [1,3,2,4],
          [1,2,4,3],
          [1,2,3,4]
        ]
      ],
    ];
  }
  public function partitionListProvider() {
    return [
      [
        3,
        [
          [3],
          [2,1],
          [1,1,1]
        ]
      ],
      [
        4,
        [
          [4],
          [3,1],
          [2,2],
          [2,1,1],
          [1,1,1,1]
        ]
      ],
    ];
  }

  /**
   * @dataProvider permeateListProvider
   */
  public function testPermeate($permeateList, $completeList) {
    $checkList = $completeList;
    sort($checkList);
    $actualList = MathStuff::permeate($permeateList);
    sort($actualList);

    $this->assertEquals(
      $checkList,
      $actualList
    );
  }

  /**
   * @dataProvider partitionListProvider
   */
  public function testPartition($partitionNumber, $completeList) {
    $checkList = $completeList;
    sort($checkList);
    $actualList = MathStuff::partition($partitionNumber);
    sort($actualList);

    $this->assertEquals(
      $checkList,
      $actualList
    );
  }
}

?>
