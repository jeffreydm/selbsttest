<?php
require_once __DIR__ . '/../' . 'classes/Range.php';
require_once __DIR__ . '/../' . 'classes/Coverage.php';

use PHPUnit\Framework\TestCase;

class CoverageTest extends TestCase {

  public function checkListProvider() {
    return [
      [
        new Range(20,40),[
          [false, new Range(0,19)],
          [false, new Range(41,50)],
          [true, new Range(10,20)],
          [true, new Range(10,23)],
          [true, new Range(20,23)],
          [true, new Range(22,24)],
          [true, new Range(25,50)],
          [true, new Range(40,50)]
        ]
      ],
      [
        new Range(0,20),[
          [false, new Range(21,30)],
          [true, new Range(0,0)],
          [true, new Range(0,10)],
          [true, new Range(10,30)]
        ]
      ],
    ];
  }
  public function additionProvider() {
    return [
      [
        //to add
        [
          new Range(10,20)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(20,20)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(21,21)
        ]
      ],
      [
        //to add
        [
          new Range(10,20),
          new Range(20,30)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(30,30)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(31,31)
        ]
      ],
      [
        //to add
        [
          new Range(10,20),
          new Range(15,30)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(30,30)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(31,31)
        ]
      ],
      [
        //to add
        [
          new Range(10,20),
          new Range(10,30)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(30,30)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(31,31)
        ]
      ],
      [
        //to add
        [
          new Range(10,20),
          new Range(21,30)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(30,30)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(31,31)
        ]
      ],
      [
        //to add
        [
          new Range(10,15),
          new Range(20,30),
          new Range(14,21)
        ],
        //to conflict
        [
          new Range(10,10),
          new Range(30,30)
        ],
        //to not conflict
        [
          new Range(9,9),
          new Range(31,31)
        ],
        [
          //to add
          [
            new Range(10,15),
            new Range(20,30),
            new Range(16,19)
          ],
          //to conflict
          [
            new Range(10,10),
            new Range(30,30)
          ],
          //to not conflict
          [
            new Range(9,9),
            new Range(31,31)
          ]
        ]
      ]
    ];
  }
  public function removalProvider() {
    return [
      [
        //start
        new Range(30,50),
        //to remove
        [
          new Range(20,40)
        ],
        //to conflict
        [
          new Range(41,41),
          new Range(50,50)
        ],
        //to not conflict
        [
          new Range(40,40),
          new Range(51,51)
        ]
      ],
      [
        //start
        new Range(30,50),
        //to remove
        [
          new Range(30,40)
        ],
        //to conflict
        [
          new Range(41,41),
          new Range(50,50)
        ],
        //to not conflict
        [
          new Range(40,40),
          new Range(51,51)
        ]
      ],
      [
        //start
        new Range(30,50),
        //to remove
        [
          new Range(40,60)
        ],
        //to conflict
        [
          new Range(30,30),
          new Range(39,39)
        ],
        //to not conflict
        [
          new Range(29,29),
          new Range(40,40)
        ]
      ],
      [
        //start
        new Range(30,50),
        //to remove
        [
          new Range(40,50)
        ],
        //to conflict
        [
          new Range(30,30),
          new Range(39,39)
        ],
        //to not conflict
        [
          new Range(29,29),
          new Range(40,40)
        ]
      ],
      [
        //start
        new Range(30,60),
        //to remove
        [
          new Range(41,49)
        ],
        //to conflict
        [
          new Range(30,30),
          new Range(40,40),
          new Range(50,50),
          new Range(60,60)
        ],
        //to not conflict
        [
          new Range(29,29),
          new Range(41,41),
          new Range(49,49),
          new Range(61,61)
        ]
      ]
    ];
  }

  public function testConstructor() {
    $c = new Coverage();
    $this->assertInstanceOf(
      Coverage::class,
      $c
    );
  }

  public function testIsEmpty() {
    $c = new Coverage();
    $this->assertEquals(
      true,
      $c->isEmpty()
    );
  }

  /**
   * @dataProvider checkListProvider
   */
  public function testCheck($range, $checkList) {
    $c = new Coverage(false);
    $c->add($range);
    foreach ($checkList as $pair) {
      $this->assertEquals(
        $pair[0],
        $c->isThereACollision($pair[1])
      );
    }
  }

  /**
   * @dataProvider additionProvider
   */
  public function testAdd($addList, $badList, $goodList) {
    $this->assertEquals(
      false,
      empty($addList)
    );
    $this->assertEquals(
      false,
      empty($badList)
    );
    $this->assertEquals(
      false,
      empty($goodList)
    );
    $c = new Coverage(false);
    foreach($addList as $additive) {
      $c->add($additive);
    }
    foreach ($badList as $bad) {
      $this->assertEquals(
        true,
        $c->isThereACollision($bad)
      );
    }
    foreach ($goodList as $good) {
      $this->assertEquals(
        false,
        $c->isThereACollision($good)
      );
    }
  }

  /**
   * @dataProvider removalProvider
   */
   public function testRemove($inital, $removeList, $badList, $goodList) {
     $this->assertInstanceOf(
       Range::class,
       $inital
     );
     $this->assertEquals(
       false,
       empty($removeList)
     );
     $this->assertEquals(
       false,
       empty($badList)
     );
     $this->assertEquals(
       false,
       empty($goodList)
     );

     $c = new Coverage(false);
     $c->add($inital);
     foreach($removeList as $removative) {
       $c->remove($removative);
     }
     foreach ($badList as $bad) {
       $this->assertEquals(
         true,
         $c->isThereACollision($bad)
       );
     }
     foreach ($goodList as $good) {
       $this->assertEquals(
         false,
         $c->isThereACollision($good)
       );
     }
   }
}

?>
