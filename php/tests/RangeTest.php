<?php
require_once __DIR__ . '/../' . 'classes/Range.php';

use PHPUnit\Framework\TestCase;

class RangeTest extends TestCase {

  public function invalidConstructorArgs()
  {
    return [
      [1,0],
      [-1,2],
      [-2,-1]
    ];
  }
  public function invalidSetterArgs()
  {
    return [
      [-1]
    ];
  }

  public function testGoodConstruct() {
    $range = new Range(0,2);
      $this->assertInstanceOf(
        Range::class,
        $range
      );
  }

  /**
   * @dataProvider invalidConstructorArgs
   */
  public function testBadConstruct($min, $max) {
    $this->expectException(\InvalidArgumentException::class);
    $range = new Range($min,$max);
  }

  public function testGet() {
    $min = 2;
    $max = 4;
    $range = new Range($min, $max);

    $this->assertEquals(
      $min,
      $range->getMin()
    );
    $this->assertEquals(
      $max,
      $range->getMax()
    );
  }

  public function testSet() {
    $min = 2;
    $max = 4;
    $range = new Range(0, 0);
    $range->setMax($max);
    $range->setMin($min);

    $this->assertEquals(
      $min,
      $range->getMin()
    );
    $this->assertEquals(
      $max,
      $range->getMax()
    );
  }

  public function testBadSetMinHigherThanMax() {
    $this->expectException(\InvalidArgumentException::class);
    $min = 0;
    $max = 2;
    $newMin = $max+2;
    $range = new Range($min, $max);
    $range->setMin($newMin);
  }

  public function testBadSetMaxLowerThanMin() {
    $this->expectException(\InvalidArgumentException::class);
    $min = 2;
    $max = 4;
    $newMax = $min-2;
    $range = new Range($min, $max);
    $range->setMax($newMax);
  }

  /**
   * @dataProvider invalidSetterArgs
   */
  public function testBadSetMin($newMin) {
    $this->expectException(\InvalidArgumentException::class);
    $range = new Range(2, 4);
    $range->setMin($newMin);
  }

  /**
   * @dataProvider invalidSetterArgs
   */
  public function testBadSetMax($newMax) {
    $this->expectException(\InvalidArgumentException::class);
    $range = new Range(2, 4);
    $range->setMax($newMax);
  }
}

?>
