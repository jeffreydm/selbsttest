<?php

session_start();
require_once '../php/classes/Membership.php';
$membership = new Membership();

// If the user clicks the "Log Out" link on the index page.
if(isset($_GET['status']) && $_GET['status'] == 'loggedout') {
 $membership->log_User_Out();
}

// Did the user enter a password/username and click submit?
if($_POST && !empty($_POST['username']) && !empty($_POST['pwd'])) {
 $response = $membership->validate_User($_POST['username'], $_POST['pwd']);
}
?>

<!DOCTYPE html>
<html id="html">
  <head>
    <meta charset="utf-8">

    <title>Selbst-Test Login</title>

    <!--main css file-->
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <!--css file for login-->
    <link href="style.css" rel="stylesheet" type="text/css">

    <meta name = "viewport" content = "width = device-width, initial-scale = 1.0, user-scalable = no">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>
    <div id="wrapperStart">
        <img id="logo" src="/bilder/logo-idee_small.png"></img>
        <form method="post" action="">
          <input class="username" type="text" placeholder="Benutzername" name="username" required>
          <input class="password" type="password" placeholder="Passwort" name="pwd" required>
          <button class="loginButton" type="submit">Login</button>
          <!--<span class="passwortVergessen"><a href="#">Forgot password?</a></span>-->
        </form>

        <?php
          if(isset($response)) echo "<h4 class='alert'>" . $response . "</h4>";
        ?>

    </div>
  </body>
</html>
